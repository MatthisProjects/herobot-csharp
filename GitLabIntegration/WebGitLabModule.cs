﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using HeroBot.Services.BotManagerService;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Unosquare.Labs.EmbedIO;
using Unosquare.Labs.EmbedIO.Constants;
using Unosquare.Labs.EmbedIO.Modules;

namespace HeroBot.Plugins.GitLab
{
    public class WebGitLabModule : WebApiController
    {
        private static readonly TaskQueue asyncExecutor = new TaskQueue();
        private static CancellationToken cancelationToken = new CancellationToken();
        private static readonly ConcurrentDictionary<string, ulong>
            taskRemove = new ConcurrentDictionary<string, ulong>();

        public static System.Collections.Concurrent.ConcurrentDictionary<string,
            ulong
        >
            d2 =
                new System.Collections.Concurrent.ConcurrentDictionary<string,
                    ulong
                >();

        public WebGitLabModule(IHttpContext context) :
            base(context)
        {
        }

        [WebApiHandler(HttpVerbs.Post, "/herobot/pl/gitlab/")]
        public async Task<bool> onGitlabCall()
        {
            string s = Request.Headers.Get("X-Gitlab-Event");
            switch (s)
            {
                case "Push Hook":
                    return await onPushHook();
                case "Tag Push Hook":
                    return await onTagPushHook();
                case "Issue Hook":
                    return await onIssueHook();
                case "Note Hook":
                    return await onNoteHook();
                case "Merge Request Hook":
                    return await onMergeRequest();
                case "Wiki Page Hook":
                    return await onPageHook();
                case "Job Hook":
                    return await onBuildHook();
                default:
                    throw
                        new EntryPointNotFoundException("Can't find action " +
                            s);
            }
        }

        [WebApiHandler(HttpVerbs.Get, "/herobot/pl/gitlab/")]
        public async Task<bool> onGetGitlabCall()
        {
            string s = Request.Headers.Get("X-Gitlab-Event");
            switch (s)
            {
                case "Push Hook":
                    return await onPushHook();
                case "Tag Push Hook":
                    return await onTagPushHook();
                case "Issue Hook":
                    return await onIssueHook();
                case "Note Hook":
                    return await onNoteHook();
                case "Merge Request Hook":
                    return await onMergeRequest();
                case "Wiki Page Hook":
                    return await onPageHook();
                case "Job Hook":
                    return await onBuildHook();
                default:
                    throw
                        new EntryPointNotFoundException("Can't find action " +
                            s);
            }
        }

        private async Task<bool> onBuildHook()
        {
            BuildHook bh = BuildHook.FromJson(this.RequestBody());
            string g = "";
            switch (bh.BuildStatus)
            {
                case "running":
                    g = "Running <:ci_running:525268381398466572>";
                    break;
                case "pending":
                    g = "Pending <:ci_waiting:525268381717233684>";
                    break;
                case "success":
                    g = "Success <:ci_ok:525268381759176714> ";
                    break;
                case "failed":
                    g = "Failed <:ci_error:525268380849274889>";
                    break;
                case "canceled":
                    g = "Cancelled <:ci_canceled:525268380794486813>";
                    break;
                case "skipped":
                    g = "Skipped <:ci_skipped:525268380949807114>";
                    break;
                case "created":
                    g = "Created <:ci_pending:525268381113516032>";
                    break;
                default:
                    g = "Unknown 🤔";
                    break;
            }
            SocketTextChannel d = await findChannel();
            if (d == null)
            {
                throw new ArgumentException("Can't find channel !");
            }

            lock (d2)
            {

                if (d2.ContainsKey(bh.Commit.Id.ToString()))
                {
                    string idL = bh.Commit.Id.ToString();
                    SocketUserMessage mess = (SocketUserMessage)d.GetMessageAsync(d2[idL]).Result;
                    if (mess != null)
                    {
                        if (mess.Embeds.FirstOrDefault() != null)
                        {
                            EmbedBuilder messageEmbed =
                                new EmbedBuilder();
                            bool contains = false;
                            bool terminated = true;

                            foreach (EmbedFieldBuilder
                                field
                                in
                                messageEmbed.Fields
                            )
                            {
                                if (
                                    field.Name.Equals(bh.BuildStage +
                                    "/" +
                                    bh.BuildName)
                                )
                                {
                                    contains = true;
                                    field.Value = g;
                                }
                                if (
                                    !(
                                    field.Value.ToString().Contains("Success") ||
                                    field.Value.ToString().Contains("Failed") ||
                                    field.Value.ToString().Contains("Cancelled") ||
                                    field.Value.ToString().Contains("Skipped")
                                    )
                                )
                                {
                                    terminated = false;
                                }
                            }
                            if (terminated == true)
                            {
                                messageEmbed
                                .AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_tracking_finished_title", "FR_fr"),
                                HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_tracking_finished_content", "FR_fr"));
                            }
                            if (!contains)
                            {
                                messageEmbed.AddField(bh.BuildStage +
                                "/" +
                                bh.BuildName,
                                g,
                                true);
                            }
                            messageEmbed.Timestamp = DateTime.Now;
                            asyncExecutor.Enqueue(() =>
                            {
                                return mess.ModifyAsync(x =>
                                {
                                    x.Embed = messageEmbed.Build();
                                });
                            },cancelationToken).ConfigureAwait(true);
                            if (terminated == true)
                            {
                                d2.Remove<string, ulong>(idL);
                            }
                        }
                        else
                        {
                            d
                            .SendMessageAsync(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_tracking_channel_doesnt_exists", "FR_fr"));
                            d2.Remove<string, ulong>(bh.Commit.Id.ToString(
                            ));
                        }
                    }
                    else
                    {
                        d
                        .SendMessageAsync(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_tracking_message_doesnt_exists", "FR_fr"));
                        d2.Remove<string, ulong>(bh.Commit.Id.ToString());
                    }
                }
                else
                {
                    bool terminated = true;
                    EmbedBuilder messageEmbed =
                        new EmbedBuilder
                        {
                            Title =
                    "<:pipeline:525631166427168769>Build Status for " +
                    bh.Commit.Message,
                            Timestamp = DateTime.Now
                        };
                    messageEmbed
                         .WithFooter(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_tracking_message_footer", "FR_fr"));
                    if (
                        !(
                        g.Contains("Success") ||
                        g.Contains("Failed") ||
                        g.Contains("Cancelled") ||
                        g.Contains("Skipped")
                        )
                    )
                    {
                        terminated = false;
                    }

                    messageEmbed.AddField(bh.BuildStage +
                    "/" +
                    bh.BuildName,
                    g,
                    true);
                    asyncExecutor.Enqueue(() =>
                    {
                        RestUserMessage mess =
                             d.SendMessageAsync(embed: messageEmbed.Build()).Result;
                        d2[bh.Commit.Id.ToString()] = mess.Id;
                        return Task.CompletedTask;
                    },cancelationToken).ConfigureAwait(false);
                    if (terminated == true)
                    {
                        d2.Remove<string, ulong>(bh.Commit.Id.ToString());
                    }
                }

            }
            return true;
        }

        private async Task<bool> onPageHook()
        {
            WikiPageHook bh = WikiPageHook.FromJson(await this.RequestBodyAsync());
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory
                .tr("gitlab_wikihook", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_wikihook_title_link", "FR_fr").Replace("{projctName}", bh.Project.Name).Replace("{projectWikiUrl}", bh.Wiki.WebUrl.ToString()),
            HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_wikihook_content", "FR_fr"));
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onMergeRequest()
        {
            MergeRequestHook bh = MergeRequestHook.FromJson(await this.RequestBodyAsync());
            if (bh.Changes.UpdatedAt == null || bh.Changes.UpdatedAt.Length == 0
            )
            {
                EmbedBuilder d =
                    new EmbedBuilder()
                    .WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_merge_request", "FR_fr"));
                d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_merge_request_title", "FR_fr").Replace("{userName}", bh.User.Username),
                HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_merge_request_content_link", "FR_fr").Replace("{projectName}", bh.Project.Name).Replace("{projectUrl}", bh.Project.WebUrl.ToString()));

                await sendMessage(d);
            }
            return true;
        }

        private async Task<bool> onNoteHook()
        {
            string rq = await this.RequestBodyAsync();
            JObject f = JObject.Parse(rq);
            if (f["merge_request"] != null)
            {
                return await onMergeRequestComment(rq);
            }
            else if (f["issue"] != null)
            {
                return await onIssueComment(rq);
            }
            else if (f["snippet"] != null)
            {
                return await onSnippetComment(rq);
            }
            else if (f["commit"] != null)
            {
                return await onCommitComment(rq);
            }
            return true;
        }

        private async Task<bool> onCommitComment(string rq)
        {
            CommentOnCommitHook bh = CommentOnCommitHook.FromJson(rq);
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_commit", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_commit_msg", "FR_fr").Replace("{userName}", bh.User.Name).Replace("{commitId}", bh.Commit.Id.Substring(0, 5))
            ,
            "[View](" + bh.Commit.Url + ")");
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onSnippetComment(string rq)
        {
            CommentOnCodeSnippetHook bh = CommentOnCodeSnippetHook.FromJson(rq);
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_snippet", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_sippet_msg", "FR_fr").Replace("{userName}", bh.User.Name).Replace("{snippet}", bh.Snippet.Title),
            "[View](" + bh.Project.WebUrl + "/snippets/" + bh.Snippet.Id + ")");
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onIssueComment(string rq)
        {
            CommentOnIssueHook bh = CommentOnIssueHook.FromJson(rq);
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_issue", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_issue_msg", "FR_fr").Replace("{userName}", bh.User.Name).Replace("{issue}", bh.Issue.Title),
            "[View](" + bh.Project.WebUrl + "/issues/" + bh.Issue.Iid + ")");
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onMergeRequestComment(string rq)
        {
            CommentOnMergeRequestHook bh =
                CommentOnMergeRequestHook.FromJson(rq);
            EmbedBuilder d =
                new EmbedBuilder()
                .WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_merge_request", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_new_comm_merge_request_msg", "FR_fr").Replace("{userName}", bh.User.Name).Replace("{mergeRequest}", bh.MergeRequest.Title),
            "[View](" +
            bh.Project.WebUrl +
            "/merge_requests/" +
            bh.MergeRequest.Iid +
            ")");
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onIssueHook()
        {
            IssueHook bh = IssueHook.FromJson(await this.RequestBodyAsync());
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_issue_hook", "FR_fr"));
            d.AddField(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_issue_hook_title", "FR_fr"),
            HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_issue_hook_msg", "FR_fr").Replace("{projectName}", bh.Project.Name).Replace("{userName}", bh.User.Name));
            await sendMessage(d);
            return true;
        }

        private async Task<bool> onTagPushHook()
        {
            TagHook bh = TagHook.FromJson(await this.RequestBodyAsync());
            EmbedBuilder d =
                new EmbedBuilder().WithTitle("🔖  New tag pushed !");
            string msgr = "";
            int add = 0;
            int rm = 0;
            int md = 0;
            foreach (CommitElement c in bh.Commits)
            {
                md = c.Modified.Length + md;
                add = c.Added.Length + add;
                rm = c.Removed.Length + rm;
                msgr += "[`" + c.Id.Substring(0, 5) + "`](" + c.Url + ")\r";
            }
            d.AddField(bh.UserName +
            " tagged " +
            bh.TotalCommitsCount +
            " commit(s) as " +
            bh.Ref,
            "Total Commits : \r` ➕" +
            add +
            " ➖" +
            rm +
            " ✔" +
            md +
            "`\r" +
            msgr,
            false);
            await sendMessage(d).ConfigureAwait(false);
            return true;
        }

        public async Task<bool> onPushHook()
        {
            PushHook bh = PushHook.FromJson(await this.RequestBodyAsync());
            EmbedBuilder d =
                new EmbedBuilder().WithTitle(HeroBot.TraduCharp.TraduCharpFactory.tr("gitlab_push_hook", "FR_fr"));
            string msg = "";
            foreach (CommitElement c in bh.Commits)
            {
                msg +=
                "\r [`" +
                c.Id.Substring(0, 5) +
                "`](" +
                c.Url +
                ") \r`" +
                c.Message +
                "`\r` ➕" +
                c.Added.Length +
                " ➖" +
                c.Removed.Length +
                " ✔" +
                c.Modified.Length +
                "`";
            }
            d.AddField(bh.UserUsername +
            " pushed " +
            bh.TotalCommitsCount +
            " commit(s)",
            msg,
            false);
            await sendMessage(d);
            return true;
        }

        public async Task<bool> sendMessage(EmbedBuilder emb)
        {
            try
            {
                string pass = Request.Headers.Get("X-Gitlab-Token");
                ulong channelId =
                    ulong.Parse(Request.QueryString.Get("channel"));
                foreach (BotService
                    bot
                    in
                    BotsManagerService.bots
                )
                {
                    foreach (DiscordSocketClient
                        client
                        in
                        bot.GetDiscord().Shards
                    )
                    {
                        try
                        {
                            SocketTextChannel c =
                                (SocketTextChannel)client.GetChannel(channelId);
                            emb.WithFooter("GitLab plugin for HeroBot");
                            if (!c.Topic.Contains(GitLabService.SHA512(pass)))
                            {
                                return await this.JsonResponseAsync("{\"error\": 401}");
                            }

                            await c.SendMessageAsync(embed: emb.Build());
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                return await this.JsonResponseAsync("{\"ok\":\"send\"}");
            }
            catch (Exception e)
            {
                return await this.JsonExceptionResponseAsync(e);
            }
        }

        public async Task<SocketTextChannel> findChannel()
        {
            try
            {
                string pass = Request.Headers.Get("X-Gitlab-Token");
                ulong channelId =
                    ulong.Parse(Request.QueryString.Get("channel"));
                foreach (BotService
                    bot
                    in
                    BotsManagerService.bots
                )
                {
                    foreach (DiscordSocketClient
                        client
                        in
                        bot.GetDiscord().Shards
                    )
                    {
                        try
                        {
                            SocketTextChannel c =
                                (SocketTextChannel)client.GetChannel(channelId);
                            if (c.Topic.Contains(GitLabService.SHA512(pass)))
                            {
                                return c;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
    }

    public static class ConcurrentDictionaryEx
    {
        public static bool
        Remove<TKey, TValue>(
            this ConcurrentDictionary<TKey, TValue> self,
            TKey key
        )
        {
            return ((IDictionary<TKey, TValue>)self).Remove(key);
        }
    }
}