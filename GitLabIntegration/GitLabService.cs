﻿using Discord;
using Discord.WebSocket;
using HeroBot.Runtime;
using HeroBot.Runtime.Entities;
using HeroBot.Services.CommandsService;
using HeroBot.Services.WebServerService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unosquare.Labs.EmbedIO.Modules;

namespace HeroBot.Plugins.GitLab
{
    public class GitLabService : Service
    {
        public GitLabService(ServicesManager sm, Plugin plugin) :
            base(sm, plugin)
        {
        }

        public override void disable()
        {
        }

        public override void postEnable()
        {
            WebServerService d =
                GetServicesManager()
                    .GetService<WebServerService>();
            d.GetWebServerInstance()
            .Module<WebApiModule>()
            .RegisterController<WebGitLabModule>();
            CommandDictionary command =
                GetServicesManager()
                    .GetService<CommandDictionary>();
            command.Register(this, GetPlugin());
            log.Info("Enabling GitLab server service");
        }

        public override void enable()
        {
        }

        [
            Command(
                "setupGitlab",
                permission = new[] { GuildPermission.ManageChannels },
                description = "!c-setupgitlab-description",
                permissionsNeed = new[] { GuildPermission.ManageChannels, GuildPermission.SendMessages, GuildPermission.EmbedLinks },
                isGuildOnly = true)
        ]
        public async Task<bool>
        onGitlabSetup(SocketUserMessage messageCreated_eventArgs)
        {
            string pass = Guid.NewGuid().ToString();
            string secret = SHA512(pass);
            await
                ((SocketTextChannel)messageCreated_eventArgs.Channel).ModifyAsync(x =>
                {
                    x.Topic = x.Topic + " [Gitlab(" + secret + ")]";
                });
            try
            {
                await
                    messageCreated_eventArgs.Channel.SendMessageAsync(embed
                    :
                    HeroUtils.messageForCommand(new Response("GitLab", TraduCharpFactory.tr("c-setupgitlab-success", messageCreated_eventArgs.Channel, new Dictionary<string, string>()
                    {
                        ["link"] = "https://api.alivecreation.fr/api/pl/gitlab?channel=" + messageCreated_eventArgs.Channel.Id,
                        ["token"] = secret
                    }
                    )),
                    messageCreated_eventArgs.Author,
                    ";setupGitlab", GetPlugin()).Build());
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            return true;
        }

        public static string SHA512(string input)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
            using (System.Security.Cryptography.SHA512 hash = System.Security.Cryptography.SHA512.Create())
            {
                byte[] hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte
                System.Text.StringBuilder hashedInputStringBuilder =
                    new System.Text.StringBuilder(128);
                foreach (byte b in hashedInputBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                }

                return hashedInputStringBuilder.ToString();
            }
        }
    }
}