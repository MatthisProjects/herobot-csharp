﻿using HeroBot.Runtime.Entities;
using System;
namespace HeroBot.Plugins.GitLab
{
    public class GitLabPlugin : Plugin
    {
        public override string getName()
        {
            return "GitLab Plugin";
        }

        public override Type[] getServices()
        {
            return
                new Type[] {
                    typeof (GitLabService)
                };
        }

        public override string getDescription()
        {
            return "[WIP]Grace a ce plugin vous pouvez lier votre GitLab a HeroBot";
        }
    }
}