﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HeroBot.Plugins.GitLab
{
    /// <summary>
    /// Permet d'eviter la surcharge d'api en évitant trop d'executions de tasks en meme temps
    /// </summarpy>
    public sealed class TaskQueue : IDisposable
    {
        private SemaphoreSlim semaphore;

        public TaskQueue() : this(degreesOfParallelism: 1)
        { }

        public TaskQueue(int degreesOfParallelism)
        {
            semaphore = new SemaphoreSlim(degreesOfParallelism);
        }

        public async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator, CancellationToken token)
        {
            await semaphore.WaitAsync(token);
            try
            {
                return await taskGenerator();
            }
            finally
            {
                semaphore.Release();
            }
        }

        public async Task Enqueue(Func<Task> taskGenerator, CancellationToken token)
        {
            await semaphore.WaitAsync(token);
            try
            {
                await taskGenerator();
            }
            finally
            {
                semaphore.Release();
            }
        }

        public void Dispose()
        {
            semaphore.Dispose();
        }
    }
}

