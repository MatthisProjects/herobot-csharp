﻿namespace HeroBot.Services.BotManagerService
{
    using Discord;
    using Discord.WebSocket;
    using DiscordBotsList.Api;
    using DiscordBotsList.Api.Objects;
    using HeroBot.basicServices.apiService;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Utils;
    using HeroBot.Services.ApiManagerService;
    using Newtonsoft.Json;
    using Prometheus;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Timers;

    /// <summary>
    /// Defines the <see cref="BotService" />
    /// </summary>
    public sealed class BotService : Service
    {

        Histogram HeroBotLatency = Metrics.CreateHistogram("herobot_latency", "",new HistogramConfiguration() { LabelNames = new[] {"ShardId" } });

        /// <summary>
        /// Defines the DblApi
        /// </summary>
        internal AuthDiscordBotListApi DblApi = null;

        /// <summary>
        /// Defines the me
        /// </summary>
        internal IDblSelfBot me;

        /// <summary>
        /// Defines the ent
        /// </summary>
        internal string token;

        /// <summary>
        /// Gets or sets the discord
        /// </summary>
        internal DiscordShardedClient discord { get; set; }

        const int shardcount = 2;

        public BotService(ServicesManager sm, Plugin plugin) : base(sm, plugin)
        {
            token = sm.GetService<ApiManagerService>().GetToken();

            try
            {
                DblApi = new AuthDiscordBotListApi(491673480006205461, sm.GetService<ApiManagerService>().GetDBLToken());
            }
            catch (Exception e)
            {
                log.Error("Failed DBL Starting", e);
            }
        }


        /// <summary>
        /// The Discord_Log
        /// </summary>
        /// <param name="arg">The arg<see cref="Discord.LogMessage"/></param>
        /// <returns>The <see cref="Task"/></returns>
        private Task DiscordLogHandler(Discord.LogMessage arg)
        {
            if (arg.Exception != null) {
                Sentry.SentrySdk.CaptureException(arg.Exception);
                Console.WriteLine(arg.Exception);
            }
            switch (arg.Severity)
            {
                case Discord.LogSeverity.Error:
                    Logger.Log.Error(arg.Message, arg.Exception);
                    break;
                case Discord.LogSeverity.Critical:
                    Logger.Log.Fatal(arg.Message, arg.Exception);
                    break;
                case Discord.LogSeverity.Warning:
                    Logger.Log.Warning(arg.Message, arg.Exception);
                    break;
                default:
                    Logger.Log.Information(arg.Message, arg.Exception);
                    break;
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// The GetName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string GetName() => "Discord.NET Bot Service System";

        /// <summary>
        /// The GetDiscord
        /// </summary>
        /// <returns>The <see cref="DiscordShardedClient"/></returns>
        public DiscordShardedClient GetDiscordClient()
        {
            return discord;
        }

        public override void Disable()
        {
            log.Information("Shutting down");
            discord.LogoutAsync().Wait();
            discord.Dispose();
        }

        public override void Enable()
        {
            discord = new DiscordShardedClient(new DiscordSocketConfig()
            {
                TotalShards = shardcount,
                MessageCacheSize = 10
            });
            discord.ShardLatencyUpdated += (newLatency, oldLatency, client) =>
            {
                HeroBotLatency.WithLabels(new[] { client.ShardId.ToString() }).Observe(newLatency);
                return Task.CompletedTask;
            };
            discord.Log += DiscordLogHandler;

            discord.ShardConnected += (shard) => {
                log.Information($"Connected shard {shard.ShardId}/{shardcount}");
                return Task.CompletedTask;
            };
            discord.ShardDisconnected += (exception,shard) =>
            {
                Sentry.SentrySdk.CaptureException(exception);
                return Task.CompletedTask;
            };
            log.Information("Trying to connect to discord");
            discord.LoginAsync(Discord.TokenType.Bot, token).Wait();
            log.Information("Démarrage en cours...");
            discord.StartAsync().Wait();
            log.Information("Bots bien démarrés");
        }
    }
}
