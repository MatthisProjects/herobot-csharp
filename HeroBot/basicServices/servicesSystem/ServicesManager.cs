﻿namespace HeroBot.Runtime
{
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Exceptions;
    using HeroBot.Runtime.Utils;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading;

    /// <summary>
    /// Defines the <see cref="ServicesManager" />
    /// </summary>
    public class ServicesManager : HeroMainLogger
    {
        /// <summary>
        /// Defines the posibleArgs
        /// </summary>
        internal Dictionary<Type, object> posibleArgs;

        /// <summary>
        /// Defines the services
        /// </summary>
        internal Dictionary<Type, Service> services;

        /// <summary>
        /// Defines the plugin
        /// </summary>
        public List<Plugin> plugin = new List<Plugin>();

        /// <summary>
        /// Defines the isRunning
        /// </summary>
        internal bool isRunning = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesManager"/> class.
        /// </summary>
        public ServicesManager()
        {
            posibleArgs = new Dictionary<Type, object>();
            services = new Dictionary<Type, Service>();
            posibleArgs.Add(GetType(), this);
            LoadPlugins();
            loadPlugins();
        }

        internal void Wait()
        {
            Thread.Sleep(-1);
        }

        /// <summary>
        /// The PostEnable
        /// </summary>
        internal void PostEnable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Post-Enabling " + s.Key.Name);
                    s.Value.PostEnable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                }
            }
        }

        /// <summary>
        /// The enable
        /// </summary>
        internal void enable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Enabling " + s.Key.Name);
                    s.Value.Enable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                    s.Value.OnError(e);
                }
            }
        }

        /// <summary>
        /// The preEnable
        /// </summary>
        internal void preEnable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Pre-Enabling " + s.Key.Name);
                    s.Value.PreEnable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                    s.Value.OnError(e);
                }
            }
        }

        /// <summary>
        /// The postDisable
        /// </summary>
        internal void postDisable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Post-Disabling " + s.Key.Name);
                    s.Value.PostDisable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                    s.Value.OnError(e);
                }
            }
        }

        /// <summary>
        /// The disable
        /// </summary>
        internal void disable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Disabling " + s.Key.Name);
                    s.Value.Disable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                    s.Value.OnError(e);
                }
            }
        }

        /// <summary>
        /// The preDisable
        /// </summary>
        internal void preDisable()
        {
            foreach (KeyValuePair<Type, Service> s in services.ToList())
            {
                try
                {
                    log.Information("Pre-Disable " + s.Key.Name);
                    s.Value.PreDisable();
                }
                catch (Exception e)
                {
                    log.Error("The Service " + s.Key.Name + " can't be loaded ",
                    e);
                    s.Value.OnError(e);
                }
            }
        }

        /// <summary>
        /// The LoadPlugins
        /// </summary>
        internal void LoadPlugins()
        {
            // Load the DLLs from the Plugins directory
            if (Directory.Exists("Plugins"))
            {
                string[] files = Directory.GetFiles("Plugins");
                foreach (string file in files)
                {
                    if (file.EndsWith(".dll"))
                    {
                        if (!file.EndsWith("HeroBot.dll"))
                        {
                            Assembly.LoadFile(Path.GetFullPath(file));
                            log.Information("Loaded DLL File " +
                            Path.GetFullPath(file));
                        }
                    }
                }
            }
            else
            {
                log.Warning("Hey the plugins folder doesn't exists" + Path.GetDirectoryName("./"));
            }

            var interfaceType = typeof(Plugin);

            // Fetch all types that implement the interface IPlugin and are a class
            Type[] types =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetTypes())
                    .Where(p =>
                        interfaceType.IsAssignableFrom(p) &&
                        p.IsClass &&
                        !p.IsAbstract)
                    .ToArray();
            List<KeyValuePair<Plugin, Type>> toLoad =
                new List<KeyValuePair<Plugin, Type>>();
            foreach (Type type in types)
            {
                try
                {
                    var pl =
                        (Plugin)Activator.CreateInstance(type, new object[] { });
                    plugin.Add(pl);
                    foreach (Type s in pl.GetServices())
                        toLoad.Add(new KeyValuePair<Plugin, Type>(pl, s));
                }
                catch (Exception e)
                {
                    log.Error("Can't invoke a plugin constructor !" + type.Name);
                }
            }

            foreach (KeyValuePair<Plugin, Type> type in toLoad)
            {
                var r = type.Value.GetConstructors().First();
                ParameterInfo[] cons_types = r.GetParameters();
                Dictionary<Type, object> posibleArgs =
                    new Dictionary<Type, object>(this.posibleArgs);
                posibleArgs.Add(typeof(Plugin), type.Key);
                List<object> objects = new List<object>();
                foreach (ParameterInfo pi in cons_types)
                {
                    if (posibleArgs.ContainsKey(pi.ParameterType))
                    {
                        objects.Add(posibleArgs[pi.ParameterType]);
                    }
                }

                try
                {
                    var s =
                        (Service)
                        Activator.CreateInstance(type.Value, objects.ToArray());
                    log.Information("Loaded lib " +
s.GetType().Name +
" from plugin " +
type.Key.GetName());
                    services.Add(s.GetType(), s);
                }
                catch (Exception e)
                {
                    log.Error("Can't invoke a service constructor !" + type.Value.Name + " ec:" + e);
                }
            }
        }

        /// <summary>
        /// The unLoadPlugins
        /// </summary>
        internal void unLoadPlugins()
        {
            preDisable();
            disable();
            postDisable();
        }

        /// <summary>
        /// The loadPlugins
        /// </summary>
        internal void loadPlugins()
        {
            preEnable();
            enable();
            PostEnable();
        }

        /// <summary>
        /// The GetService
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>The <see cref="T"/></returns>
        public T GetService<T>() where T : Service
        {
            if (services.ContainsKey(typeof(T))) return (T)services[typeof(T)];
            throw new ServiceNotFoundException();
        }
    }
}
