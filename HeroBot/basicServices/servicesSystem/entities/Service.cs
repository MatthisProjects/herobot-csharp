﻿namespace HeroBot.Runtime.Entities
{
    using HeroBot.Runtime.Utils;
    using System;

    /// <summary>
    /// Defines the <see cref="Service" />
    /// </summary>
    public abstract class Service : HeroMainLogger
    {
        /// <summary>
        /// Defines the sm
        /// </summary>
        internal ServicesManager sm;

        /// <summary>
        /// Defines the pl
        /// </summary>
        internal Plugin pl;

        /// <summary>
        /// Initializes a new instance of the <see cref="Service"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public Service(ServicesManager sm, Plugin plugin)
        {
            this.sm = sm;
            pl = plugin;
        }

        /// <summary>
        /// The GetServicesManager
        /// </summary>
        /// <returns>The <see cref="ServicesManager"/></returns>
        public ServicesManager GetServicesManager() => sm;

        /// <summary>
        /// The GetPlugin
        /// </summary>
        /// <returns>The <see cref="Plugin"/></returns>
        public Plugin GetPlugin() => pl;

        /// <summary>
        /// The disable
        /// </summary>
        public abstract void Disable();

        /// <summary>
        /// The enable
        /// </summary>
        public abstract void Enable();

        /// <summary>
        /// The onError
        /// </summary>
        /// <param name="e">The e<see cref="Exception"/></param>
        public virtual void OnError(Exception e)
        {
            log.Error("Exception throwed in a Service !", e); Console.WriteLine(e);
        }

        /// <summary>
        /// The preEnable
        /// </summary>
        public virtual void PreEnable()
        {
        }

        /// <summary>
        /// The postEnable
        /// </summary>
        public virtual void PostEnable()
        {
        }

        /// <summary>
        /// The preDisable
        /// </summary>
        public virtual void PreDisable()
        {
        }

        /// <summary>
        /// The postDisable
        /// </summary>
        public virtual void PostDisable()
        {
        }
    }
}
