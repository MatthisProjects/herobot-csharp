﻿namespace HeroBot.Runtime.Entities
{
    using HeroBot.Runtime.Utils;
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Plugin" />
    /// </summary>
    public abstract class Plugin : HeroMainLogger
    {
        /// <summary>
        /// Defines the name
        /// </summary>
        internal string
            name = "*[DEPRECATED]*Unknown Plugin #" + new Random().Next();

        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public virtual Type[] GetServices() => new Type[] { };

        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public virtual string GetName()
        {
            log.Warning("No name for plugin " + GetType().Name);
            return name;
        }

        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public virtual string GetDescription()
        {
            log.Warning("No description for plugin " + GetType().Name);
            return "*" + name + " : No description*";
        }
    }
}
