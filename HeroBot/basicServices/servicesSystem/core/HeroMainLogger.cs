﻿namespace HeroBot.Runtime.Utils
{
    using HeroBot.basicServices.apiService;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="HeroMainLogger" />
    /// </summary>
    public abstract class HeroMainLogger
    {
        /// <summary>
        /// Defines the log
        /// </summary>
        protected static readonly Serilog.Core.Logger log = Logger.Log;
    }
}
