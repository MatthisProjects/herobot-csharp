﻿namespace HeroBot.Runtime.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="ServiceAlereadyExistsException" />
    /// </summary>
    internal sealed class ServiceAlereadyExistsException : Exception
    {
    }
}
