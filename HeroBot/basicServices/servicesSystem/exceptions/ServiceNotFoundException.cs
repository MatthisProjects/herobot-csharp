﻿namespace HeroBot.Runtime.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="ServiceNotFoundException" />
    /// </summary>
    internal sealed class ServiceNotFoundException : Exception
    {
    }
}
