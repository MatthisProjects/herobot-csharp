﻿namespace HeroBot.Runtime.Entities.Traduction
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="LanguageObject" />
    /// </summary>
    internal class LanguageObject
    {
        /// <summary>
        /// Defines the obj_language
        /// </summary>
        public string obj_language;

        /// <summary>
        /// Defines the word
        /// </summary>
        public Dictionary<string, string> word;
    }
}
