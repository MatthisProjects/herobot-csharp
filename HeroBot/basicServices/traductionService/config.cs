﻿namespace HeroBot.Runtime.Entities.Traduction
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Config" />
    /// </summary>
    internal class Config
    {
        /// <summary>
        /// Defines the languagesLocation, defaultLanguage
        /// </summary>
        public string languagesLocation, defaultLanguage;
    }
}
