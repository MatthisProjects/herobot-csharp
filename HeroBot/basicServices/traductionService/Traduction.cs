﻿namespace HeroBot.Runtime.Entities.Traduction
{
    using Discord.WebSocket;
    using HeroBot.basicServices.apiService;
    using HeroBot.Runtime.Entities;
    using HeroBot.Services.ApiManagerService;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Traduction" />
    /// </summary>
    internal class Traduction : Service
    {
        /// <summary>
        /// Defines the config
        /// </summary>
        private static Config config = null;

        /// <summary>
        /// Defines the languages
        /// </summary>
        private static ConcurrentDictionary<string, Dictionary<string, string>> languages = new ConcurrentDictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Defines the apiM
        /// </summary>
        private static ApiManagerService apiM;

        /// <summary>
        /// Defines the THIS
        /// </summary>
        internal static Traduction THIS = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Traduction"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="pl">The pl<see cref="Plugin"/></param>
        public Traduction(ServicesManager sm, Plugin pl) :
            base(sm, pl)
        {
            apiM = sm.GetService<ApiManagerService>();
            THIS = this;
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            var configFileLocations = @"languages/config.json";
            lock (languages)
            {
                if (!File.Exists(configFileLocations))
                {
                    throw new ArgumentException("Invalid TraduCharp configuration");
                }
                try
                {
                    config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(configFileLocations));
                    if (config.defaultLanguage == null || config.languagesLocation == null)
                    {
                        throw new InvalidOperationException("Invalid config file !");
                    }
                }
                catch (Exception e)
                {
                    throw new InvalidDataException("Can't load the TraduCharp config");
                }
                if (!Directory.Exists(config.languagesLocation))
                {
                    throw new InvalidOperationException("The languages directory doesn't exists");
                }
                foreach (string s in Directory.GetFiles(config.languagesLocation))
                {
                    if (!File.Exists(s))
                    {
                        throw new FileLoadException("Internal exception : " + s + " doesn't exists !");
                    }
                    try
                    {
                        LanguageObject l = JsonConvert.DeserializeObject<LanguageObject>(File.ReadAllText(s));
                        log.Information("Loaded language extension " + l.word.Count + " word(s)");
                        if (!languages.ContainsKey(l.obj_language))
                        {
                            languages[l.obj_language] = new Dictionary<string, string>();
                        }
                        foreach (KeyValuePair<string, string> value in l.word)
                        {
                            if (!value.Key.StartsWith("_"))
                            {
                                languages[l.obj_language][value.Key] = value.Value;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log.Warning("Failed to load language " + s, e);
                    }
                }
            }
        }

        /// <summary>
        /// The Traduct
        /// </summary>
        /// <param name="value">The value<see cref="string"/></param>
        /// <param name="keyValuePairs">The keyValuePairs<see cref="Dictionary{string, string}"/></param>
        /// <param name="socketUser">The socketUser<see cref="SocketUser"/></param>
        /// <param name="socketGuild">The socketGuild<see cref="SocketGuild"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string Traduct(string value, Dictionary<string, string> keyValuePairs, SocketUser socketUser, SocketGuild socketGuild)
        {
            // Try to get user / guild language

            // First we try to get the user attached language via website login
            //var user = apiM.GetUser()
            string language = null;

            // The language field is not implemented yet
            if (language == null || !languages.Keys.Contains(language))
            {
                //var guild = apiM.getDatabase().GetGuild((long)socketUser.Id);
                //language = guild.lang ?? config.defaultLanguage;
                language = config.defaultLanguage;
            }
            // Now if the user had a prefered language we have to use it


            // If the selected language doesn't exists, use the default language
            if (!languages.Keys.Contains(language))
                language = config.defaultLanguage;
            // If the default language doesn't exists, return a error string
            if (!languages.Keys.Contains(config.defaultLanguage))
                return "`Can't find language " + config.defaultLanguage + "`";
            // If the selected language replic doesn't exists, set to the def language
            if (!languages[language].ContainsKey(value))
                language = config.defaultLanguage;
            // If the work definitively not exists return an error string
            if (!languages[config.defaultLanguage].ContainsKey(value))
                return "`Word not found in " + config.defaultLanguage + " " + value + "`";
            // If all is ok, we can return the sentence
            var sentence = languages[language][value];

            foreach (KeyValuePair<string, string> r in keyValuePairs)
            {
                sentence = sentence.Replace("[@" + r.Key + "]", r.Value);
            }
            return sentence.Replace("[[ENV:DATE]]", Versioning.date).Replace("[[ENV:VERSION]]", Versioning.build).Replace("[[ENV:REF]]", Versioning.build);
        }
    }

    /// <summary>
    /// Defines the <see cref="StringExtension" />
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// The Traduct
        /// </summary>
        /// <param name="value">The value<see cref="string"/></param>
        /// <param name="keyValuePairs">The keyValuePairs<see cref="Dictionary{string,string}"/></param>
        /// <param name="socketGuild">The socketGuild<see cref="SocketGuild"/></param>
        /// <param name="socketUser">The socketUser<see cref="SocketUser"/></param>
        /// <returns>The <see cref="string"/></returns>
        public static string Traduct(this string value, Dictionary<string, string> keyValuePairs, SocketGuild socketGuild = null, SocketUser socketUser = null)
        {
            return Traduction.THIS.Traduct(value, keyValuePairs, socketUser, socketGuild);
        }
    }
}
