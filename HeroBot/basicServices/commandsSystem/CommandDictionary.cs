namespace HeroBot.Services.CommandsService
{
    using Discord;
    using Discord.WebSocket;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Entities.Traduction;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService.ArgsManager;
    using HeroBot_2.basicServices.commandsSystem;
    using Prometheus;
    using Sentry;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Timers;

    /// <summary>
    /// Defines the <see cref="CommandDictionary" />
    /// </summary>
    public class CommandDictionary : Service
    {
        /// <summary>
        /// Defines the ExecutedJobs
        /// </summary>
        private static readonly Counter ExecutedJobs = Metrics
    .CreateCounter("herobot_commands_executed", "Number of commands waiting processed");

        /// <summary>
        /// Defines the SuccesfullJobs
        /// </summary>
        private static readonly Counter SuccesfullJobs = Metrics
    .CreateCounter("herobot_commands_success", "Number of commands executed succesfully");

        /// <summary>
        /// Defines the FailedJobs
        /// </summary>
        private static readonly Counter FailedJobs = Metrics
    .CreateCounter("herobot_commands_failed", "Number of commands failes");
        /// <summary>
        /// Defines the ProcessTime
        /// </summary>
        private static readonly Histogram ProcessTime = Metrics
    .CreateHistogram("herobot_commands_time", "Time to process a command", new HistogramConfiguration()
    {
        LabelNames = new string[] { "command" },

    });
        /// <summary>
        /// Defines the cooldown
        /// </summary>
        internal List<ulong> cooldown = new List<ulong>();

        /// <summary>
        /// Defines the apiM
        /// </summary>
        internal ApiManagerService.ApiManagerService apiM;

        /// <summary>
        /// Defines the commandAliasses
        /// </summary>
        internal Dictionary<string, SimpleCommand>
            commandAliasses = new Dictionary<string, SimpleCommand>();

        /// <summary>
        /// Defines the posibleArgs
        /// </summary>
        internal Dictionary<Type, object>
            posibleArgs = new Dictionary<Type, object>();

        /// <summary>
        /// Defines the keyValuePairs
        /// </summary>
        public static Dictionary<string, SimpleCommand>
            keyValuePairs = new Dictionary<string, SimpleCommand>();

        /// <summary>
        /// Defines the ArgsManager
        /// </summary>
        internal ArgsManager.ArgsManager ArgsManager = new ArgsManager.ArgsManager();

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandDictionary"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="pl">The pl<see cref="Plugin"/></param>
        public CommandDictionary(ServicesManager sm, Plugin pl) :
            base(sm, pl)
        {
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            Register(new BasicCommands(GetServicesManager(), GetPlugin()), GetPlugin());
            posibleArgs.Add(GetServicesManager().GetType(), GetServicesManager());
            posibleArgs.Add(GetType(), this);
            apiM = GetServicesManager().GetService<ApiManagerService.ApiManagerService>();
            GetServicesManager().GetService<BotService>().GetDiscordClient().MessageReceived += OnMessage;
        }

        public async Task OnMessage(SocketMessage args)
        {
            try
            {
                if (args.Author.IsBot) return;
                if (
                    args.Content.StartsWith(";")
                )
                {

                    ExecutedJobs.Inc();
                    string[] command2 = args.Content.TrimStart(';').Split(" ");
                    var commandName = command2[0].ToLower();
                    string[] command = command2.Skip(1).ToArray();

                    using (ProcessTime.WithLabels(new[] { commandName }).NewTimer())
                    {
                        if (
                        keyValuePairs.ContainsKey(commandName) ||
                        commandAliasses.ContainsKey(commandName)
                    )
                        {
                            SimpleCommand c = null;
                            if (!keyValuePairs.ContainsKey(commandName))
                            {
                                c = commandAliasses[commandName];
                            }
                            else
                            {
                                c = keyValuePairs[commandName];
                            }

                            SocketGuildChannel socketGuildChannel = null;
                            socketGuildChannel = args.Channel as SocketGuildChannel;
                            if (socketGuildChannel != null && !apiM.getAdministrators().Contains(args.Author.Id))
                            {

                                var authorPerms =
                                    socketGuildChannel.Guild.GetUser(args.Author.Id);
                                var selfPerms =
                                    socketGuildChannel.Guild.GetUser(socketGuildChannel.Guild.CurrentUser.Id);
                                if (c.needPermissions != null)
                                {
                                    foreach (var permission
                                        in
                                        c.needPermissions
                                    )
                                    {
                                        if (
                                            !selfPerms.GuildPermissions.Has(permission)
                                        )
                                        {
                                            await
                                                args.Channel
                                                .SendMessageAsync("sys-insufisent-permission".Traduct(new Dictionary<string, string>() { ["permission"] = permission.ToString() }));
                                            //   await
                                            //     args
                                            //   .AddReactionAsync(this.ErrorEmote);
                                            return;
                                        }
                                    }
                                }
                                if (c.permission != null)
                                {
                                    foreach (var permission
                                            in
                                            c.permission
                                        )
                                    {
                                        if (
                                            !authorPerms.GuildPermissions.Has(permission)
                                        )
                                        {
                                            await
                                                args
                                                .Channel.SendMessageAsync("sys-user-insufisent-permission".Traduct(new Dictionary<string, string>() { ["permission"] = permission.ToString() }));
                                            //                      await
                                            //                        args
                                            //                      .AddReactionAsync(this.ErrorEmote);
                                            return;

                                        }
                                    }
                                }
                            }

                            if (c.isGuildOnly)
                            {
                                if (
                                    socketGuildChannel == null
                                )
                                {
                                    await
                                        args.Channel
                                        .SendMessageAsync("sys-command-only-server".Traduct(new Dictionary<string, string>()));
                                    //                    await
                                    //                          args
                                    //                            .AddReactionAsync(this.ErrorEmote);
                                    FailedJobs.Inc();
                                    return;
                                }
                            }

                            if (c.isAdmin)
                            {
                                if (
                                    !apiM.getAdministrators().Contains(args.Author.Id)
                                )
                                {
                                    await
                                        args.Channel
                                        .SendMessageAsync("sys-command-admin-only".Traduct(new Dictionary<string, string>()));
                                    //                          await
                                    //                                args.AddReactionAsync(this.ErrorEmote);
                                    FailedJobs.Inc();
                                    return;

                                }
                            }
                            if (cooldown.Contains(args.Author.Id))
                            {
                                await args.Channel.SendMessageAsync("sys-command-cooldown".Traduct(new Dictionary<string, string>()));
                                //                          await
                                //args.AddReactionAsync(this.ErrorEmote);
                                FailedJobs.Inc();
                                return;
                            }

                            ParameterInfo[] cons_types =
                                c.method.GetParameters();
                            List<object> objects = new List<object>();
                            Dictionary<Type, object> posibleArgs =
                                new Dictionary<Type, object>(this.posibleArgs);
                            posibleArgs.Add(args.Content.GetType(), args.Content);
                            posibleArgs.Add(typeof(CommandContext), new CommandContext() {
                                Message = args
                            });
                            posibleArgs.Add(apiM.GetType(), apiM);
                            try
                            {

                                c.method.Invoke(c.instance, ArgsManager.MethodPhaseArguments(";" + commandName, string.Join(" ", command), c.method, posibleArgs, args as SocketUserMessage));
                                // await
                                //   args.AddReactionAsync(this.SuccessEmote);
                                SuccesfullJobs.Inc();
                                cooldown.Add(args.Author.Id);
                                await System.Threading.Tasks.Task.Factory.StartNew(() =>
                                {
                                    Thread.Sleep(3000);
                                    cooldown.Remove(args.Author.Id);
                                });

                            }
                            catch (Exception exxx)
                            {
                                if (exxx is InvalidArgument || exxx is TargetParameterCountException)
                                {
                                    Console.WriteLine("Error !");
                                    await args.Channel.SendMessageAsync("Syntaxe invalide ! Voici la démarche a suivre **;" + c.name + " " + c.syntax + "**");
                                    FailedJobs.Inc();
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                var exs = "Rapport d'erreur d'HeroBot \r" + exc.ToString() + "\r Dans le channel : " + args.Channel.Name;
                MemoryStream s = new MemoryStream(Encoding.ASCII.GetBytes(exs));
                await
                  args.Channel.SendFileAsync(s, "ErrorReport.txt", "sys-error".Traduct(new Dictionary<string, string>() { ["link"] = "https://alivecreation.fr" }));
                FailedJobs.Inc();
            }
        }

        /// <summary>
        /// The showMatch
        /// </summary>
        /// <param name="text">The text<see cref="string"/></param>
        /// <param name="expr">The expr<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        internal static bool showMatch(string text, string expr)
        {
            var mc = Regex.Matches(text, expr);
            return mc.ToArray().Length > 0;
        }

        /// <summary>
        /// The Register
        /// </summary>
        /// <param name="o">The o<see cref="object"/></param>
        /// <param name="pl">The pl<see cref="Plugin"/></param>
        public void Register(object o, Plugin pl)
        {
            foreach (MethodInfo info in o.GetType().GetMethods())
            {
                var isAttribued = false;
                var commandName = "";
                var sc = new SimpleCommand();
                foreach (Attribute
                    attribute
                    in
                    info.GetCustomAttributes<Command>()
                )
                {
                    var c = (Command)attribute;

                    if (isAttribued)
                    {
                        commandAliasses.Add(c.name.ToLower(), sc);
                        keyValuePairs[commandName].aliasses.Add(c.name.ToLower());
                    }
                    else
                    {
                        sc.description = c.description;
                        sc.instance = o;
                        sc.method = info;
                        sc.name = c.name.ToLower();
                        sc.permission = c.permission;
                        sc.needPermissions = c.permissionsNeed;
                        sc.isGuildOnly = c.isGuildOnly;
                        sc.plugin = pl;
                        sc.isAdmin = c.isAdmin;
                        isAttribued = true;
                        sc.isHidden = c.isHidden;
                        sc.syntax = ArgsManager.MethodToStringExplain(sc.method);
                        commandName = c.name;
                        keyValuePairs.Add(c.name.ToLower(), sc);

                    }
                }
            }
        }

        /// <summary>
        /// Defines the commandExecuted
        /// </summary>
        public static int commandExecuted;
    }
}
