﻿using Discord.Rest;
using Discord.WebSocket;
using HeroBot.Runtime.Entities.Traduction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HeroBot_2.basicServices.commandsSystem
{
    public class CommandContext
    {
        public SocketMessage Message { get; internal set; }

        public Task<RestUserMessage> Reply(string e,Dictionary<string,string> key = null) {
            return Message.Channel.SendMessageAsync(e.Traduct(key, socketUser: Message.Author));
        }
    }
}
