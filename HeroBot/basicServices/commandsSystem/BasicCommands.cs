﻿namespace HeroBot.Services.CommandsService
{
    using Discord;
    using Discord.WebSocket;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Entities.Traduction;
    using HeroBot.Services.BotManagerService;
    using HeroBot_2.basicServices.commandsSystem;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Loader;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="BasicCommands" />
    /// </summary>
    internal class BasicCommands
    {
        /// <summary>
        /// Defines the dm
        /// </summary>
        private readonly ServicesManager dm;

        /// <summary>
        /// Defines the Plugin
        /// </summary>
        private readonly Plugin Plugin;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicCommands"/> class.
        /// </summary>
        /// <param name="dm">The dm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public BasicCommands(ServicesManager dm, Plugin plugin)
        {
            this.dm = dm;
            Plugin = plugin;
        }

        /// <summary>
        /// The OnLeaveCommand
        /// </summary>
        /// <param name="__eventArgs">The __eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [
            Command(
                "quit",
                permission = new[] { GuildPermission.Administrator },
                description = "!command-quit-description",
                isGuildOnly = true)
        ]
        public async void OnLeaveCommand(
            CommandContext __eventArgs
        )
        {
            var ev = __eventArgs.Message.Channel as SocketGuildChannel;

            await __eventArgs.Reply("command-quit-reply");
            await ev.Guild.LeaveAsync();
        }

        /// <summary>
        /// The OnHelpCommand
        /// </summary>
        /// <param name="_event">The _event<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="command">The command<see cref="String"/></param>
        [
            Command(
                "help",
                description = "!command-help-description",
                permissionsNeed =
                new[] { GuildPermission.EmbedLinks })
        ]
        [Command("h")]
        [Command("hh")]
        [Command("pm")]
        [Command("aide")]
        [Command("helphere")]
        public async void OnHelpCommand(

            CommandContext _event,

            [Ignore]String command = ""
        )
        {
            var commandsData = new Dictionary<Plugin, List<SimpleCommand>>();
            // For each command
            foreach (var data in CommandDictionary.keyValuePairs)
            {
                // check if the command is a hidden command

                if (!commandsData.ContainsKey(data.Value.plugin))
                {
                    commandsData.Add(data.Value.plugin, new List<SimpleCommand>());
                }

                commandsData[data.Value.plugin].Add(data.Value);

            }
            if (command == "")
            {
                var r = new Random();
                var emb = new EmbedBuilder();
                emb.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
                emb.Title = "command-help-title".Traduct(new Dictionary<string, string>());
                emb.AddField("**HeroBot**",
                "HeroBot is a bot with a plugin system that can be developped by the community, maintained by **Matthieu#2050**, it is supposed to be able to replace several bots. \n [ [_**Our Discord**_](https://alivecreation.fr/discord) | [_**Website 📟**_](https://herobot.alivecreation.fr) ] \n [ [_**Invite HeroBot 😍**_](https://alivecreation.fr/invite) | [**Upvote us on DBL 🎉**](https://discordbots.org/bot/491673480006205461) ]\r **[] is a required argument, <> is an optional argument** If your argument contains spaces, you must put it behind two `\"`",
                false);
                emb.WithDescription("Hay ! Thanks for using HeroBot ! I hope you enjoy using our bot :P, is you have any suggestions, you can tell us your beautiful idea !");
                emb.WithTitle("HeroBot Help");
                emb.WithFooter("Pro tip : You can use ;hh to send the message in the current channel");
                emb.WithThumbnailUrl("https://cdn.discordapp.com/avatars/491673480006205461/30abe7a1feffb0b06a1611a94fbc1248.png");

                foreach (KeyValuePair<Plugin, List<SimpleCommand>> f in commandsData)
                {
                    var s = "";
                    foreach (SimpleCommand simpleCommand in f.Value)
                    {
                        if (!simpleCommand.isHidden)
                        {
                            var desc = simpleCommand.description;
                            var name = simpleCommand.name;
                            s += "**>**  `" + name + " " + simpleCommand.syntax + "`  ";
                        }
                    }
                    var name2 = f.Key.GetName();
                    if (name2.StartsWith("!"))
                    {
                        //name2 = name2.Replace("!", "").Traduct(new Dictionary<string, string>(), ((_event.Message as SocketUserMessage).Channel as SocketGuildChannel));
                    }

                    emb.AddField(name2, s, true);
                }

                if ((_event.Message.Channel as SocketGuildChannel) != null)
                {
                    if (_event.Message.Content.StartsWith(";hh") || _event.Message.Content.StartsWith(";helphere"))
                    {
                        await _event.Message.Channel.SendMessageAsync(embed: emb.Build(), text: "Voici l'aide " + _event.Message.Author.Mention);
                    }
                    else
                    {
                        await _event.Message.Author.SendMessageAsync(embed: emb.Build());
                    }
                }
                else
                {
                    await _event.Message.Author.SendMessageAsync(embed: emb.Build());
                }
            }
            else
            {
                var plugins = commandsData.Where(a => a.Value.Where(x => x.name == command).FirstOrDefault() != null);

                if (plugins.Count() != 0)
                {
                    var commands = plugins.First().Value.Where(x =>
                    {

                        return x.name == command;
                    }).First();
                    if (commands == null)
                    {
                        if ((_event.Message.Channel as SocketGuildChannel) != null)
                        {
                            if (_event.Message.Content.StartsWith(";hh") || _event.Message.Content.StartsWith(";helphere"))
                            {
                                await _event.Message.Channel.SendMessageAsync("Je n'ai pas trouvé cette commande !2");
                            }
                            else
                            {
                                await _event.Message.Author.SendMessageAsync("Je n'ai pas trouvé cette commande !2");
                            }
                        }
                        else
                        {
                            await _event.Message.Author.SendMessageAsync("Je n'ai pas trouvé cette commande !2");
                        }
                        return;
                    }
                    EmbedBuilder emb = new EmbedBuilder();
                    var r = new Random();
                    emb.AddField("**HeroBot**",
                    "HeroBot is a bot with a plugin system that can be developped by the community, maintained by **Matthieu#2050**, it is supposed to be able to replace several bots. \n [ [_**Our Discord**_](https://alivecreation.fr/discord) | [_**Website 📟**_](https://herobot.alivecreation.fr) ] \n [ [_**Invite HeroBot 😍**_](https://alivecreation.fr/invite) | [**Upvote us on DBL 🎉**](https://discordbots.org/bot/491673480006205461) ]\r **[] is a required argument, <> is an optional argument**",
                    false);
                    emb.WithDescription("Hay ! Thanks for using HeroBot ! I hope you enjoy using our bot :P, is you have any suggestions, you can tell us your beautiful idea !");
                    emb.WithTitle("HeroBot Help");
                    emb.WithFooter("Pro tip : You can use ;hh to send the message in the current channel");
                    emb.WithThumbnailUrl("https://cdn.discordapp.com/avatars/491673480006205461/30abe7a1feffb0b06a1611a94fbc1248.png");

                    emb.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
                    emb.AddField("Syntaxe", "**>**  `" + commands.name + " " + commands.syntax + "`");
                    emb.WithTitle("Aide de la commande " + commands.name);
                    emb.AddField("Permissions requises ( bot )", this.ListPToString(commands.needPermissions) == "" ? "*aucune*" : this.ListPToString(commands.needPermissions), true);
                    emb.AddField("Permission requises ( utilisateur )", this.ListPToString(commands.permission) == "" ? "*aucune*" : this.ListPToString(commands.permission), true);
                    emb.AddField("Commande admin ?", commands.isAdmin ? "Oui" : "Non", true);
                    emb.AddField("Commande cachée ?", commands.isHidden ? "Oui" : "Non", true);
                    emb.AddField("Commande aliasse ?", commands.isAlias ? "Oui" : "Non", true);
                    emb.AddField("Description", commands.description, true);
                    emb.AddField("Aliasses", this.ListToString(commands.aliasses) == "" ? "*aucune*" : this.ListToString(commands.aliasses), true);
                    if ((_event.Message.Channel as SocketGuildChannel) != null)
                    {
                        if (_event.Message.Content.StartsWith(";hh") || _event.Message.Content.StartsWith(";helphere"))
                        {
                            await _event.Message.Channel.SendMessageAsync(embed: emb.Build(), text: "Voici l'aide " + _event.Message.Author.Mention);
                        }
                        else
                        {
                            await _event.Message.Author.SendMessageAsync(embed: emb.Build());
                        }
                    }
                    else
                    {
                        await _event.Message.Author.SendMessageAsync(embed: emb.Build());
                    }
                }
                else
                {
                    if ((_event.Message.Channel as SocketGuildChannel) != null)
                    {
                        if (_event.Message.Content.StartsWith(";hh") || _event.Message.Content.StartsWith(";helphere"))
                        {
                            await _event.Message.Channel.SendMessageAsync("Je n'ai pas trouvé cette commande !");
                        }
                        else
                        {
                            await _event.Message.Author.SendMessageAsync("Je n'ai pas trouvé cette commande !");
                        }
                    }
                    else
                    {
                        await _event.Message.Author.SendMessageAsync("Je n'ai pas trouvé cette commande !");
                    }
                }
            }
        }

        /// <summary>
        /// The getValue
        /// </summary>
        /// <param name="cmds">The cmds<see cref="Dictionary{string, KeyValuePair{Plugin, List{SimpleCommand}}}"/></param>
        /// <param name="v">The v<see cref="string"/></param>
        /// <returns>The <see cref="KeyValuePair{string, KeyValuePair{Plugin, List{SimpleCommand}}}?"/></returns>
        internal KeyValuePair<string, KeyValuePair<Plugin, List<SimpleCommand>>>?
        getValue(
            Dictionary<string, KeyValuePair<Plugin, List<SimpleCommand>>> cmds,
            string v
        )
        {
            foreach (KeyValuePair<string,
                    KeyValuePair<Plugin, List<SimpleCommand>>
                >
                ex
                in
                cmds
            )
            {
                if (ex.Key.ToLower().Replace(" ", "").Equals(v))
                {
                    return ex;
                }
            }

            return null;
        }

        /// <summary>
        /// The inListContains
        /// </summary>
        /// <param name="e">The e<see cref="Dictionary{string, KeyValuePair{Plugin, List{SimpleCommand}}}"/></param>
        /// <param name="s">The s<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        internal bool
        inListContains(
            Dictionary<string, KeyValuePair<Plugin, List<SimpleCommand>>> e,
            string s
        )
        {
            foreach (KeyValuePair<string,
                    KeyValuePair<Plugin, List<SimpleCommand>>
                >
                ex
                in
                e
            )
            {
                if (ex.Key.ToLower().Replace(" ", "").Equals(s))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The ListPToString
        /// </summary>
        /// <param name="permission">The permission<see cref="GuildPermission[]"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string ListPToString(GuildPermission[] permission)
        {
            var r = "";
            var isFirst = true;
            foreach (GuildPermission s in permission)
            {
                if (!isFirst)
                {
                    r += ", ";
                }
                else
                {
                    r += "\r";
                }

                r += s;
                isFirst = false;
            }

            return r;
        }

        /// <summary>
        /// The OnInviteCommand
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [
            Command(
                "support",
                permission = new GuildPermission[] { },
                description = "!command-support-description",
                permissionsNeed =
                new[]
                {
                    GuildPermission.EmbedLinks,GuildPermission.SendMessages
                })
        ]
        [Command("server")]
        [Command("discord")]
        [Command("invite")]
        public async void OnInviteCommand(CommandContext _eventArgs)
        {
            await _eventArgs.Reply(
                    "command-support-reply", new Dictionary<string, string>()
                    {
                        ["link"] = "https://alivecreation.fr/discord"
                    });
        }

        /// <summary>
        /// The OnPluginsCommand
        /// </summary>
        /// <param name="_e">The _e<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="_services">The _services<see cref="ServicesManager"/></param>
        [
            Command(
                "pl",
                permission = new[] { GuildPermission.Administrator },
                description = "!command-plugin-description",
                permissionsNeed =
                new[] { GuildPermission.SendMessages })
        ]
        [Command("plugins")]
        [Command("plist")]
        [Command("pluginslist")]
        public async void OnPluginsCommand(
            CommandContext _e,
            ServicesManager _services
        )
        {
            var emb = new EmbedBuilder();
            Random r = new Random();
            emb.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
            emb.Title = "command-plugin-embed-title".Traduct(new Dictionary<string, string>());
            foreach (Plugin pl in _services.plugin)
            {
                var t = pl.GetName();
                var d = pl.GetDescription();
                if (t.StartsWith("!")) { t = "`" + t.Replace("!", "").Traduct(new Dictionary<string, string>()) + "`"; }
                if (d.StartsWith("!")) { d = "`" + d.Replace("!", "").Traduct(new Dictionary<string, string>()) + "`"; }
                emb.AddField(t, d, false);
            }
            await _e.Message.Channel.SendMessageAsync(embed: emb.Build());
        }

        /// <summary>
        /// The GuildListCommand
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="services">The services<see cref="ServicesManager"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "searchServer",
                permission = new GuildPermission[] { },
                description = "RESERVE AU CREATEUR DE BOT",
                isAdmin = true,
                isHidden = true)
        ]
        public async Task
        GuildListCommand(CommandContext e, ServicesManager services)
        {
            var name = e.Message.Content.Replace(";searchServer ", "");
            BotService se =
                services.GetService<BotService>();
            var msg = "Aucune guilde :/";
            var emb = new EmbedBuilder();
            emb.WithTitle("Recherche result :");
                foreach (SocketGuild
                    guild
                    in
                    se.GetDiscordClient().Guilds
                )
                {
                    if (guild.Name.ToLower().Contains(name.ToLower()))
                    {
                        if (msg == "Aucune guilde :/")
                        {
                            msg = "";
                        }

                        var r = "Pas d'invitation :/";
                        try
                        {
                            r = (await guild.DefaultChannel.CreateInviteAsync()).Url;
                        }
                        catch (Exception) { }
                        emb.AddField(guild.Name, "[Invitation](" + r + ")");
                    }
            }
            await e.Message.Channel.SendMessageAsync(embed: emb.Build());
        }

        /// <summary>
        /// The ListToString
        /// </summary>
        /// <param name="mist">The mist<see cref="List{string}"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string ListToString(List<string> mist)
        {
            if (mist.Count > 0)
            {
                var r = "";
                var isFirst = true;
                foreach (string s in mist)
                {
                    if (!isFirst)
                    {
                        r += ", ";
                    }

                    r += s;
                    isFirst = false;
                }

                return r;
            }

            return "";
        }

        /// <summary>
        /// The OnUserLeave
        /// </summary>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns>
        internal async Task OnUserLeave(SocketGuildUser e)
        {
            var emb = new EmbedBuilder
            {
                Title =
            "e-user-left-title".Traduct(new Dictionary<string, string>()
            {
                ["user#tag"] = e.Username + "#" + e.Discriminator
            }, e.Guild),
                Description =
            "e-user-left-message".Traduct(new Dictionary<string, string>()
            {
                ["server"] = e.Guild.Name
            }, e.Guild)
            };
            Random r = new Random();
            emb.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
            foreach (SocketChannel c in e.Guild.TextChannels)
            {
                if (((SocketTextChannel)c).Topic != null && ((SocketTextChannel)c).Topic.Contains("[HeroWELCOME]"))
                {
                    if (((SocketTextChannel)c).Topic.Contains("[HeroWELCOME]"))
                    {
                        await ((SocketTextChannel)c).SendMessageAsync(embed: emb.Build());
                    }
                }
            }

        }

        /// <summary>
        /// The OnUserJouin
        /// </summary>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns>
        internal async Task OnUserJoin(SocketGuildUser e)
        {
            var emb = new EmbedBuilder
            {
                Title =
            "e-user-join-title".Traduct(new Dictionary<string, string>()
            {
                ["user#tag"] = e.Username + "#" + e.Discriminator
            }, e.Guild),
                Description =
            "e-user-join-message".Traduct(new Dictionary<string, string>()
            {
                ["server"] = e.Guild.Name,
                ["user#mention"] = e.Username + "#" + e.Discriminator
            }, e.Guild)
            };
            Random r = new Random();
            emb.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
            foreach (SocketChannel c in e.Guild.TextChannels)
            {
                if (c.GetType().Equals(typeof(SocketTextChannel)))
                {
                    if (((SocketTextChannel)c).Topic != null && ((SocketTextChannel)c).Topic.Contains("[HeroWELCOME]"))
                    {
                        await ((SocketTextChannel)c).SendMessageAsync(embed: emb.Build());
                    }
                }
            }

        }

        private async Task OnGuildJoin(SocketGuild _eventArgs)
        {

            await _eventArgs.DefaultChannel.SendMessageAsync("Hey bien le bonjour, je suis HeroBot a votre service ! Pour avoir de l'aide tape `;help` mon préfixe est `;`");

            var botService = this.dm.GetService<BotManagerService.BotService>().GetDiscordClient();
            var guild = botService.Guilds.Where(x => x.Id == 494850247600635904).FirstOrDefault();
            if (guild != null) {
                await guild.GetTextChannel(579320728663359498).SendMessageAsync($":tada: Nouveau serveur rejoins ! `{_eventArgs.Name.Replace("`",String.Empty)}`, {_eventArgs.MemberCount} membres, crée par {_eventArgs.Owner.Username}#{_eventArgs.Owner.Discriminator}");
            }
        }

        /// <summary>
        /// The onAbout
        /// </summary>
        /// <param name="__eventArgs">The __eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [
            Command(
                "about",
                description = "!command-about-description",
                isAdmin = false,
                isGuildOnly = false,
                isHidden = false,
                permission = new GuildPermission[] { },
                permissionsNeed =
                new[] { GuildPermission.SendMessages, GuildPermission.EmbedLinks })
        ]
        public async Task onAbout(CommandContext __eventArgs)
        {
            await __eventArgs.Reply(
                    "command-about-reply", new Dictionary<string, string>()
                    {
                        ["link"] = "https://alivecreation.fr/api/"
                    });
        }

        /// <summary>
        /// The OnServerInfo
        /// </summary>
        /// <param name="eventArgs">The eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [Command("serverinfo", isGuildOnly = true)]
        [Command("guildinfo")]
        [Command("gi")]
        [Command("si")]
        public async Task OnServerInfo(CommandContext _eventArgs)
        {
            SocketGuild socketGuild = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            EmbedBuilder embedBuilder = new EmbedBuilder();
            var r = new Random();
            embedBuilder.WithTitle(socketGuild.Name).WithDescription("Voici quelques information a propos de " + socketGuild.Name);
            embedBuilder.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
            embedBuilder.AddField("Membres", socketGuild.MemberCount + " membres(s)", false);
            var rolesString = "";
            var i = 0;
            foreach (SocketRole socketRole in socketGuild.Roles)
            {
                if (rolesString.Length != 250)
                {
                    rolesString += socketRole.Mention + ",";
                    i++;
                }
                else
                {
                    rolesString += ("**et " + (socketGuild.Roles.Count - i) + " autres**");
                }
            }
            embedBuilder.AddField("Roles", rolesString, false);
            var channelString = "";
            i = 0;
            foreach (SocketTextChannel socketRole in socketGuild.TextChannels)
            {
                if (channelString.Length != 250)
                {
                    channelString += socketRole.Mention + ",";
                    i++;
                }
                else
                {
                    channelString += ("**et " + (socketGuild.TextChannels.Count - i) + " autres**");
                }
            }
            embedBuilder.AddField("Channels textuels", channelString, false);
            channelString = "";
            i = 0;
            foreach (SocketVoiceChannel socketRole in socketGuild.VoiceChannels)
            {
                if (channelString.Length != 250)
                {
                    channelString += "`" + socketRole.ToString() + "`,";
                    i++;
                }
                else
                {
                    channelString += ("**et " + (socketGuild.TextChannels.Count - i) + " autres**");
                }
            }
            embedBuilder.AddField("Channels vocaux", channelString, false);
            channelString = "";
            i = 0;
            foreach (GuildEmote socketRole in socketGuild.Emotes)
            {
                if (channelString.Length != 250)
                {
                    if (!socketRole.Animated)
                    {
                        channelString += socketRole.ToString() + ",";
                    }
                    else
                    {
                        channelString += "`" + socketRole.Name + "`";
                    }

                    i++;
                }
                else
                {
                    channelString += ("**et " + (socketGuild.Emotes.Count - i) + " autres**");
                }
            }
            embedBuilder.AddField("Emojis", channelString == "" ? "*aucun emoji*" : channelString, false);
            embedBuilder.AddField("Créateur", socketGuild.Owner.Username + "#" + socketGuild.Owner.Discriminator, false);

            channelString = "";
            i = 0;
            var rd = socketGuild.Users.Where(x => x.GuildPermissions.Administrator || x.GuildPermissions.BanMembers || x.GuildPermissions.ManageMessages || x.GuildPermissions.MentionEveryone);
            foreach (SocketUser socketRole in rd)
            {
                if (channelString.Length != 250)
                {
                    channelString += socketRole.Mention + ",";
                    i++;
                }
                else
                {
                    channelString += ("**et des " + (rd.Count() - i) + " autres**");
                }
            }
            embedBuilder.WithCurrentTimestamp();
            embedBuilder.AddField("Staff", channelString, false);
            embedBuilder.AddField("Date de création", socketGuild.CreatedAt.ToString(), false);
            embedBuilder.AddField("Niveau de sécurité", socketGuild.VerificationLevel, false);
            embedBuilder.AddField("Channel AFK", "`" + (socketGuild.AFKChannel == null && socketGuild.AFKChannel.Name == "" ? "*aucun channel AFK*" : socketGuild.AFKChannel.Name + " ( " + socketGuild.AFKTimeout.ToString() + ")") + "`", false);
            embedBuilder.AddField("Date de join", socketGuild.CurrentUser.JoinedAt.ToString());
            embedBuilder.WithThumbnailUrl(socketGuild.IconUrl);
            await _eventArgs.Message.Channel.SendMessageAsync(embed: embedBuilder.Build());
        }

        /// <summary>
        /// The OnUserInfo
        /// </summary>
        /// <param name="eventArgs">The eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="socketUser">The socketUser<see cref="SocketUser"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [Command("userinfo")]
        [Command("ui")]
        public async Task OnUserInfo(CommandContext _eventArgs,
            SocketUser target = null)
        {
            if (target == null)
            {
                target = _eventArgs.Message.Author;
            }

            EmbedBuilder embedBuilder = new EmbedBuilder();
            var r = new Random();
            embedBuilder.WithTitle("Informations a propos de " + target.Username);
            embedBuilder.WithCurrentTimestamp();
            embedBuilder.WithThumbnailUrl(target.GetAvatarUrl());
            embedBuilder.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));

            if (((SocketGuildChannel)_eventArgs.Message.Channel) != null)
            {
                var guild = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
                var member = guild.GetUser(target.Id);
                embedBuilder.AddField("Surnom", member.Nickname == null ? "*aucun surnom*" : member.Nickname);
                embedBuilder.AddField("Muté du serveur", member.IsMuted ? "Oui" : "Non");
                embedBuilder.AddField("Sourd du serveur", member.IsDeafened ? "Oui" : "Non");
                embedBuilder.AddField("Micro activé", member.IsSelfMuted ? "Non" : "Oui");
                embedBuilder.AddField("Son activé", member.IsSelfDeafened ? "Non" : "Oui");
                embedBuilder.AddField("Date de join", member.JoinedAt.ToString());
                embedBuilder.AddField("Date de création du compte", member.CreatedAt.ToString());


                var type = "User";
                if (member.IsBot)
                {
                    type = "Bot";
                }

                if (member.IsWebhook)
                {
                    type = "Webhook";
                }

                embedBuilder.AddField("Type", type);
                embedBuilder.AddField("Mention", member.Mention);
                if (member.GuildPermissions.Administrator || member.GuildPermissions.BanMembers || member.GuildPermissions.ManageMessages || member.GuildPermissions.MentionEveryone)
                {
                    embedBuilder.AddField("Staff ?", "Oui");
                }
                else
                {
                    embedBuilder.AddField("Staff ?", "Non");
                }
            }
            embedBuilder.AddField("Serveurs en commun", target.MutualGuilds.Count + "serveurs");
            embedBuilder.AddField("Nom", target.Username + "#" + target.Discriminator);
            embedBuilder.AddField("Status", target.Status);
            if (target.Activity != null)
            {
                var IsSpotify = (target.Activity as SpotifyGame) != null;
                var IsRichGame = (target.Activity as RichGame) != null;
                var IsStreming = (target.Activity as StreamingGame) != null;
                switch (target.Activity.Type)
                {
                    case ActivityType.Playing:
                        if (IsRichGame)
                        {
                            var RichGame = (RichGame)target.Activity;
                            embedBuilder.AddField("Activity ( Rich Presence )", "Joue à " + RichGame.Name + " : " + RichGame.State + " depuis " + RichGame.Timestamps.Start
                                );
                        }
                        else
                        {
                            embedBuilder.AddField("Activity", "Joue a " + target.Activity.Name);
                        }

                        break;
                    case ActivityType.Listening:
                        if (IsSpotify)
                        {
                            var Spotify = (SpotifyGame)target.Activity;
                            embedBuilder.AddField("Activity ( Spotify )", "Ecoute [`" + Spotify.TrackTitle + "`  (" + Spotify.Duration.Value + ") ](" + Spotify.TrackUrl + ") de `" + ListToString(Spotify.Artists) + "` sur Spotify\r");
                        }
                        else
                        {
                            embedBuilder.AddField("Activity", "Ecoute " + target.Activity.Name);
                        }

                        break;
                    case ActivityType.Streaming:
                        if (IsStreming)
                        {
                            var Stream = (StreamingGame)target.Activity;
                            embedBuilder.AddField("Activity ( Streaming )", "En streaming [`" + Stream.Name + "`](" + Stream.Url + ")");
                        }
                        else
                        {
                            embedBuilder.AddField("Activity", "En straming " + target.Activity.Name);
                        }

                        break;
                    case ActivityType.Watching:
                        embedBuilder.AddField("Activity ( Regarde )", "Regarde  " + target.Activity.Name);
                        break;
                    default:
                        embedBuilder.AddField("Activity", "Aucun jeu");
                        break;
                }
            }
            else
            {
                embedBuilder.AddField("Activity", "Aucun jeu");
            }
            await _eventArgs.Message.Channel.SendMessageAsync(embed: embedBuilder.Build());
        }

        /// <summary>
        /// The ListToString
        /// </summary>
        /// <param name="artists">The artists<see cref="IReadOnlyCollection{string}"/></param>
        /// <returns>The <see cref="string"/></returns>
        private string ListToString(IReadOnlyCollection<string> artists)
        {
            String r = "";
            foreach (String s in artists)
            {
                r += " " + s;
            }
            return r;
        }
        private async Task<double> GetCpuUsageForProcess()
        {
            var startTime = DateTime.UtcNow;
            var startCpuUsage = Process.GetCurrentProcess().TotalProcessorTime;
            await Task.Delay(500);

            var endTime = DateTime.UtcNow;
            var endCpuUsage = Process.GetCurrentProcess().TotalProcessorTime;
            var cpuUsedMs = (endCpuUsage - startCpuUsage).TotalMilliseconds;
            var totalMsPassed = (endTime - startTime).TotalMilliseconds;
            var cpuUsageTotal = cpuUsedMs / (Environment.ProcessorCount * totalMsPassed);
            return cpuUsageTotal * 100;
        }

        public long getAvailableRAM()
        {
            return Process.GetCurrentProcess().WorkingSet64;
        }
        /// <summary>
        /// The Info
        /// </summary>
        /// <param name="messageReceviedEventArgs">The messageReceviedEventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [Command("botinfo")]
        [Command("bi")]
        [Command("ping")]
        public async Task Info(CommandContext _messageReceviedEventArgs)
        {

            var websocketPing = Math.Round((DateTime.Now - _messageReceviedEventArgs.Message.CreatedAt).TotalMilliseconds);
            var watch = new Stopwatch();
            watch.Start();
            var message = await _messageReceviedEventArgs.Message.Channel.SendMessageAsync("**⏲ Mesurating 0/1 ( mesuring )**");
            watch.Stop();
            var msMessageSend = watch.ElapsedMilliseconds;
            var ticksMessageSend = watch.ElapsedTicks;
            watch = new Stopwatch();
            watch.Start();
            await message.ModifyAsync(x =>
            {
                x.Content = "**⏲ Mesurating 1/1 ( evaluating results )**";
            });
            watch.Stop();
            var msMessageEdit = watch.ElapsedMilliseconds;
            var ticksMessageEdit = watch.ElapsedTicks;
            var memoryCount = "Private memory: "+SizeSuffix(Process.GetCurrentProcess().PrivateMemorySize64) + $" Working Set: {SizeSuffix(getAvailableRAM())}";
            var processor = await GetCpuUsageForProcess();
            var dotnetVersion = Environment.Version.ToString();
            var processorCount = Environment.ProcessorCount;
            await message.ModifyAsync(x =>
            {
                x.Content = _messageReceviedEventArgs.Message.Author.Mention+" <:ping:581772617481060363> Pong ! \n**Bot newtork stats : `" + (websocketPing < 1000 || msMessageSend < 1000 || msMessageEdit < 1000 ? "🆗" : "❌") + "`  ** \n Websocket ping : `" + websocketPing + "ms`\n Message send latency : `" + msMessageSend + "ms`\n Message edit latency : `" + msMessageEdit + "ms`" +
                "\n Mémoire dédiée au process : `" + memoryCount + "`\nVersion de .NET Core : `" + dotnetVersion + "`\n Nombre de processeurs: `" + processorCount + "coeurs`"+"\nUtilisation de processeur: `"+processor+"%`";
            });
        }
        /// <summary>
        /// Defines the SizeSuffixes
        /// </summary>
        internal static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        /// <summary>
        /// The SizeSuffix
        /// </summary>
        /// <param name="value">The value<see cref="Int64"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal static string SizeSuffix(Int64 value)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 bytes"; }

            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        /// <summary>
        /// The onEval
        /// </summary>
        /// <param name="_messageReceviedEventArgs">The _messageReceviedEventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("eval", isAdmin = true, isHidden = true)]
        public void onEval(CommandContext _messageReceviedEventArgs)
        {
            var dotnetCoreDirectory = Path.GetDirectoryName(typeof(object).GetTypeInfo().Assembly.Location);


            var refe = new List<MetadataReference>();

            // Toutes les refs
            // boucle pour load toutes les clasees
            refe.Add(MetadataReference.CreateFromFile(typeof(object).Assembly.Location));
            foreach (Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
            {

                try
                {
                    refe.Add(MetadataReference.CreateFromFile(ass.Location));
                }
                catch (Exception) { }
            }

            var compilation = CSharpCompilation.Create("evalAssembly")
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                .AddReferences(refe.ToArray())
                .AddSyntaxTrees(CSharpSyntaxTree.ParseText((_messageReceviedEventArgs.Message.Content.Replace(";eval", String.Empty).Replace("```cs", String.Empty).Replace("```", String.Empty))));

            var isError = false;
            var s = String.Empty;
            // Debug output. In case your environment is different it may show some messages.
            foreach (var compilerMessage in compilation.GetDiagnostics())
            {
                if (compilerMessage.WarningLevel == 0)
                {
                    isError = true;
                }

                s += "```" + compilerMessage + "```";
            }
            if (isError)
            {
                _messageReceviedEventArgs.Message.Channel.SendMessageAsync("Erreur de compilation : " + s);
            }

            using (var memoryStream = new MemoryStream())
            {
                var emitResult = compilation.Emit(memoryStream);
                if (emitResult.Success)
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    CollectibleAssemblyLoadContext loadcontext = new CollectibleAssemblyLoadContext();

                    var assembly = loadcontext.LoadFromStream(memoryStream);

                    var mathod = assembly.GetType("EvalClass").GetMethod("Eval");

                    var objects = new List<object>();
                    foreach (var param in mathod.GetParameters())
                    {
                        if (param.ParameterType.Equals(typeof(CommandContext)))
                        {
                            objects.Add(_messageReceviedEventArgs);
                        }
                    }
                    var sys = mathod.Invoke(null, objects.ToArray());
                    if (sys != null)
                    {
                        _messageReceviedEventArgs.Message.Channel.SendMessageAsync("```json\n"+JsonConvert.SerializeObject(sys)+"```");
                    }

                    loadcontext.Unload();
                    GC.Collect();
                    GC.WaitForFullGCApproach();
                    GC.Collect();
                }
            }
        }

        /// <summary>
        /// Defines the <see cref="CollectibleAssemblyLoadContext" />
        /// </summary>
        public class CollectibleAssemblyLoadContext : AssemblyLoadContext
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="CollectibleAssemblyLoadContext"/> class.
            /// </summary>
            public CollectibleAssemblyLoadContext() : base(isCollectible: true)
            {
            }

            /// <summary>
            /// The Load
            /// </summary>
            /// <param name="assemblyName">The assemblyName<see cref="AssemblyName"/></param>
            /// <returns>The <see cref="Assembly"/></returns>
        }
    }
}
