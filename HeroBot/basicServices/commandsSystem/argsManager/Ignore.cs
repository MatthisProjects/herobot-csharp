﻿namespace HeroBot.basicServices.commandsSystem.argsManager
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Ignore" />
    /// </summary>
    [
    System.AttributeUsage(
        System.AttributeTargets.Parameter,
        AllowMultiple = false) // Multiuse attribute.
]
    public class Ignore : Attribute
    {
    }
}
