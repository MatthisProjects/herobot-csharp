﻿namespace HeroBot.Services.CommandsService.ArgsManager
{
    using Discord.WebSocket;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Defines the <see cref="ArgsManager" />
    /// </summary>
    internal class ArgsManager
    {
        /// <summary>
        /// Defines the converters
        /// </summary>
        internal Dictionary<string, IConverter> converters = new Dictionary<string, IConverter>();

        /// <summary>
        /// Defines the optionnalAtributeFormat, AtributeFormat
        /// </summary>
        internal static string optionnalAtributeFormat = "<%s> ", AtributeFormat = "[%s] ";

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgsManager"/> class.
        /// </summary>
        public ArgsManager()
        {
            RegisterConverter(typeof(string), new StringConverter());
            RegisterConverter(typeof(int), new IntConverter());
            RegisterConverter(typeof(SocketUser), new User());
            RegisterConverter(typeof(SocketChannel), new Channel());
            RegisterConverter(typeof(SocketRole), new Role());
            RegisterConverter(typeof(long), new LongConverter());
        }

        /// <summary>
        /// The MethodToStringExplain
        /// </summary>
        /// <param name="method">The method<see cref="MethodInfo"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string MethodToStringExplain(MethodInfo method)
        {
            var stringBuilder = new StringBuilder();
            foreach (var argument in method.GetParameters())
            {
                if (!argument.Name.StartsWith("_"))
                {
                    var str = AtributeFormat;
                    if (argument.HasDefaultValue) str = optionnalAtributeFormat;
                    stringBuilder.Append(str.Replace("%s", argument.Name));
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// The RemoveBetween
        /// </summary>
        /// <param name="s">The s<see cref="string"/></param>
        /// <param name="begin">The begin<see cref="char"/></param>
        /// <param name="end">The end<see cref="char"/></param>
        /// <returns>The <see cref="MatchCollection"/></returns>
        internal MatchCollection RemoveBetween(string s, char begin, char end)
        {
            var reg = new Regex("\".*?\"");
            var matches = reg.Matches(s);
            return matches;
        }

        /// <summary>
        /// The StringConvert
        /// </summary>
        /// <param name="s">The s<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string StringConvert(string s)
        {
            foreach (Match find in RemoveBetween(s, '"', '"'))
            {
                s = s.Replace(find.ToString(), find.ToString().Replace("\"", string.Empty).Replace(" ", "\\^"));
            }
            foreach (Match find in RemoveBetweenQuotes(s))
            {
                s = s.Replace(find.ToString(), find.ToString().Replace("```", string.Empty).Replace(" ", "\\^"));
            }
            return s;
        }

        private IEnumerable<Match> RemoveBetweenQuotes(string s)
        {
            var reg = new Regex("```.*?````");
            var matches = reg.Matches(s);
            return matches;
        }

        /// <summary>
        /// The MethodPhaseArguments
        /// </summary>
        /// <param name="s">The s<see cref="string"/></param>
        /// <param name="methodInfo">The methodInfo<see cref="MethodInfo"/></param>
        /// <param name="pa">The pa<see cref="Dictionary{Type, object}"/></param>
        /// <param name="eventArgs">The eventArgs<see cref="SocketUserMessage"/></param>
        /// <returns>The <see cref="object[]"/></returns>
        internal object[] MethodPhaseArguments(string command, string s, MethodInfo methodInfo, Dictionary<Type, object> pa, SocketUserMessage eventArgs)
        {
            string[] args = StringConvert(s).Replace("  ", " ").Split(" ");
            string phased = String.Empty;
            List<object> objectsMethodParam = new List<object>();
            var i = 0;
            foreach (var param in methodInfo.GetParameters())
            {
                if (pa.ContainsKey(param.ParameterType) && param.GetCustomAttributes<Ignore>().Count() == 0)
                {
                    objectsMethodParam.Add(pa[param.ParameterType]);

                }
                else
                if (converters.ContainsKey(param.ParameterType.Name) && s.Length >= i)
                {
                    try
                    {
                        if (param.GetCustomAttributes<Remainer>().Count() == 1)
                        {
                            objectsMethodParam.Add(converters[param.ParameterType.Name].Phase(s.ToLower().Replace(phased, String.Empty), eventArgs));

                        }
                        else
                            objectsMethodParam.Add(converters[param.ParameterType.Name].Phase(args[i - 1], eventArgs));
                        phased += args[i - 1] + " ";
                    }
                    catch (Exception e)
                    {
                        if (param.HasDefaultValue)
                        {
                            objectsMethodParam.Add(param.DefaultValue);
                        }
                        else
                        {
                            throw new InvalidArgument();
                        }
                    }
                }
                else if (param.HasDefaultValue)
                {
                    objectsMethodParam.Add(param.DefaultValue);
                }
                else
                {
                    throw new InvalidArgument();
                }
                i++;
            }

            return objectsMethodParam.ToArray();
        }

        /// <summary>
        /// Defines the <see cref="StringConverter" />
        /// </summary>
        public class StringConverter : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args) => s.Replace("\\^", " ");
        }

        /// <summary>
        /// Defines the <see cref="IntConverter" />
        /// </summary>
        public class IntConverter : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args) => int.Parse(s);
        }

        /// <summary>
        /// Defines the <see cref="LongConverter" />
        /// </summary>
        public class LongConverter : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args) => long.Parse(s);
        }

        /// <summary>
        /// Defines the <see cref="User" />
        /// </summary>
        public class User : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args)
            {
                foreach (var u in args.MentionedUsers)
                {
                    s = s.Replace("<@", string.Empty).Replace(">", string.Empty).Replace("!", string.Empty);
                    if (u.Id == ulong.Parse(s)) return u;
                }
                return null;
            }
        }

        /// <summary>
        /// Defines the <see cref="Channel" />
        /// </summary>
        public class Channel : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args)
            {
                foreach (var u in args.MentionedChannels)
                {
                    s = s.Replace("<#", string.Empty).Replace(">", string.Empty);
                    if (u.Id == ulong.Parse(s)) return u;
                }
                return null;
            }
        }

        /// <summary>
        /// Defines the <see cref="Role" />
        /// </summary>
        public class Role : IConverter
        {
            /// <summary>
            /// The Phase
            /// </summary>
            /// <param name="s">The s<see cref="string"/></param>
            /// <param name="args">The args<see cref="SocketMessage"/></param>
            /// <returns>The <see cref="object"/></returns>
            public object Phase(string s, SocketMessage args)
            {
                foreach (var u in args.MentionedRoles)
                {
                    s = s.Replace("<@&", string.Empty).Replace(">", string.Empty);
                    if (u.Id == ulong.Parse(s)) return u;
                }
                return null;
            }
        }

        /// <summary>
        /// The RegisterConverter
        /// </summary>
        /// <param name="t">The t<see cref="Type"/></param>
        /// <param name="conv">The conv<see cref="IConverter"/></param>
        public void RegisterConverter(Type t, IConverter conv) => converters.Add(t.Name, conv);
    }

    /// <summary>
    /// Defines the <see cref="IConverter" />
    /// </summary>
    interface IConverter
    {
        /// <summary>
        /// The Phase
        /// </summary>
        /// <param name="s">The s<see cref="string"/></param>
        /// <param name="args">The args<see cref="SocketMessage"/></param>
        /// <returns>The <see cref="object"/></returns>
        object Phase(string s, SocketMessage args);
    }
}
