﻿namespace HeroBot.Services.CommandsService.ArgsManager
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="InvalidArgument" />
    /// </summary>
    internal class InvalidArgument : Exception
    {
    }
}
