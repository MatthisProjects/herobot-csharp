﻿namespace HeroBot.Services.CommandsService
{
    using Discord;
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Command" />
    /// </summary>
    [
        System.AttributeUsage(
            System.AttributeTargets.Method,
            AllowMultiple = true) // Multiuse attribute.
    ]
    public class Command : Attribute
    {
        /// <summary>
        /// Defines the name, syntax, description
        /// </summary>
        public string name, syntax, description = "*No description gived*";

        /// <summary>
        /// Defines the permission
        /// </summary>
        public GuildPermission[]
            permission = new GuildPermission[] { };

        /// <summary>
        /// Defines the permissionsNeed
        /// </summary>
        public GuildPermission[]
            permissionsNeed = new GuildPermission[] { };

        /// <summary>
        /// Defines the isGuildOnly, isAdmin, isHidden
        /// </summary>
        public bool isGuildOnly, isAdmin, isHidden;

        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        /// <param name="name">The name<see cref="string"/></param>
        public Command(string name)
        {
            this.name = name;
        }
    }
}
