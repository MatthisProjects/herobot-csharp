﻿namespace HeroBot.Services.CommandsService
{
    using Discord;
    using HeroBot.Runtime.Entities;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="SimpleCommand" />
    /// </summary>
    public class SimpleCommand
    {
        /// <summary>
        /// Defines the plugin
        /// </summary>
        public Plugin plugin;

        /// <summary>
        /// Gets or sets a value indicating whether isAlias
        /// </summary>
        public bool isAlias { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether isAdmin
        /// </summary>
        public bool isAdmin { get; set; }

        /// <summary>
        /// Gets or sets the instance
        /// </summary>
        public object instance { get; set; }

        /// <summary>
        /// Gets or sets the method
        /// </summary>
        public MethodInfo method { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether isHidden
        /// </summary>
        public bool isHidden { get; set; } = false;

        /// <summary>
        /// Gets or sets the permission
        /// </summary>
        public GuildPermission[] permission { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Gets or sets the aliasses
        /// </summary>
        public List<string> aliasses { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets a value indicating whether isGuildOnly
        /// </summary>
        public bool isGuildOnly { get; set; } = false;

        /// <summary>
        /// Gets or sets the needPermissions
        /// </summary>
        public GuildPermission[] needPermissions { get; set; }

        /// <summary>
        /// Gets or sets the syntax
        /// </summary>
        public string syntax { set; internal get; }
    }
}
