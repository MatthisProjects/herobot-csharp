﻿namespace HeroBot.Plugins
{
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Entities.Traduction;
    using HeroBot.Services.ApiManagerService;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService;
    using System;

    /// <summary>
    /// Defines the <see cref="BasicSystemPlugin" />
    /// </summary>
    class BasicSystemPlugin : Plugin
    {
        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public override Type[] GetServices()
        {
            return
                new Type[] {
                    typeof (ApiManagerService),
                    typeof (BotService),
                    typeof (CommandDictionary),
                    typeof (Traduction),
                };
        }

        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetName() => "*HeroBot System*";

        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetDescription()
        {
            return "Service basiques de HeroBot, nécessaires au fonctionnement normal du bot !";
        }
    }
}
