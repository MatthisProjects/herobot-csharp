﻿namespace HeroBot.basicServices.apiService
{
    using Serilog;
    using Serilog.Sinks.Loki;
    using Serilog.Sinks.Loki.Labels;
    using Serilog.Sinks.SystemConsole.Themes;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="Logger" />
    /// </summary>
    public static class Logger
    {

        /// <summary>
        /// Defines the Log
        /// </summary>
        public static Serilog.Core.Logger Log = initLogger();

        /// <summary>
        /// The initLogger
        /// </summary>
        /// <returns>The <see cref="Serilog.Core.Logger"/></returns>
        public static Serilog.Core.Logger initLogger()
        {
            return new LoggerConfiguration()
        .MinimumLevel.Information()
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .CreateLogger();
        }
    }
}
