﻿
using HeroBot.basicServices.apiService.Entities;
using HeroBot_2.basicServices.apiService.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroBot.basicServices.apiService
{
    public class HeroBotContext : DbContext
    {

        public DbSet<Guild> Guilds { get; set; }

        public DbSet<GuildConfig> GuildConfigs { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<GuildUser> GuildUsers { get; set; }

        public DbSet<Warn> Warns { get; set; }
        public HeroBotContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }
    }
}
