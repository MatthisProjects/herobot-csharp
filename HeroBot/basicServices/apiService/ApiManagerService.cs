namespace HeroBot.Services.ApiManagerService
{
    using Discord;
    using Discord.WebSocket;
    using HeroBot.basicServices.apiService;
    using HeroBot.basicServices.apiService.Entities;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService.ArgsManager;
    using HeroBot_2.basicServices.apiService;
    using HeroBot_2.basicServices.apiService.Entities;
    using HeroBot_2.basicServices.commandsSystem;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ApiManagerService" />
    /// </summary>
    public class ApiManagerService : Service
    {
        /// <summary>
        /// Defines the cooldown
        /// </summary>
        internal List<ulong> cooldown = new List<ulong>();

        /// <summary>
        /// Defines the admins
        /// </summary>
        internal List<ulong> admins = new List<ulong>();

        internal string GetToken()
        {
            var token = Environment.GetEnvironmentVariable("TOKEN");
            Environment.SetEnvironmentVariable("TOKEN", "x-x-x-x-x-x-x-x-x");
            return token;
        }
        internal string GetDBLToken() {
            var token = Environment.GetEnvironmentVariable("DBL_TOKEN");
            Environment.SetEnvironmentVariable("DBL_TOKEN", "x-x-x-x-x-x-x-x-x");
            return token;
        }
        private DbContextOptions DbContextOptions = new DbContextOptionsBuilder().UseNpgsql("Server=ssh.alivecreation.fr;Port=30001;Database=herobot;User Id=herobot_runtime;Password=AZBTAyZuk9YpXf3mzZm9Qtef3txBw8fHJHc9R3eN2P2QGTM6wJ5b2V3FdqSk8XDuwFtdjhRbXHCvcZbyz6kcCdHsdrCXYRgm9kahuR53ah8c89ByQLmjATAEUNdJwkFfvF9z85s8cqWW996QwgYWLqcgPfBar82wqZVpFc7JdTgcDcDZ5tyFuAy8FYTGWK3cmNbSC8cFDju77XH226h6wF4Mvy54TXR6nSzAQBR5wb3tKm5BhcjqHc3NnWG6wpMJ;Pooling=true;MinPoolSize=1;MaxPoolSize=20;").Options;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiManagerService"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="pl">The pl<see cref="Plugin"/></param>
        public ApiManagerService(ServicesManager sm, Plugin pl) :
            base(sm, pl)
        {
            
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
           
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            this.GetServicesManager().GetService<CommandsService.CommandDictionary>().Register(this, GetPlugin());
            log.Information("Connecting to postgres");
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                log.Information("Manually creating database ;)");
                try
                {
                    heroBotContext.Database.ExecuteSqlRawAsync("CREATE DATABASE herobot;").Wait();
                    heroBotContext.Database.ExecuteSqlRawAsync("create extension hstore;").Wait();
                }
                catch (Exception e) { }
                log.Information("...Done ! Migrating !");
                heroBotContext.Database.Migrate();
                log.Information("Succesfully migrated");
            }
        }

        /// <summary>
        /// The postEnable
        /// </summary>
        public override void PostEnable()
        {
            var discordCllient = GetServicesManager().GetService<BotService>().GetDiscordClient();
            discordCllient.GuildAvailable += OnGuildAvailable;
            discordCllient.JoinedGuild += GuildJoinied;
            discordCllient.LeftGuild += LeftGuildEventArgs;
            discordCllient.MessageReceived += MessageReceviedEventArgs;
            discordCllient.ShardConnected += CheckclientGuilds;
        }

        /// <summary>
        /// The getAdministrators
        /// </summary>
        /// <returns>The <see cref="ulong[]"/></returns>
        public ulong[] getAdministrators()
        {
            return new ulong[] { 288683683664363530L, 455300291739516928L, 406135526005932043L, 263713074630885376L, 397413447664664577L, 312573691571732480L, 314354049023737857L };
        }

        public Task OnGuildAvailable(SocketGuild e)
        {
            CreateGuild(e);
            return Task.CompletedTask;
        }
        public Task GuildJoinied(SocketGuild e)
        {
            CreateGuild(e);
            return Task.CompletedTask;
        }

        public Task CheckclientGuilds(DiscordSocketClient discordSocketClient) {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                foreach (SocketGuild d in discordSocketClient.Guilds)
                {
                    if (heroBotContext.Guilds.Where(x => x.DiscordId == d.Id).Count() == 0) {
                        CreateGuild(d);
                    }
                }
                return heroBotContext.SaveChangesAsync();
            }
        }

        public Task LeftGuildEventArgs(SocketGuild e)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                heroBotContext.Guilds.Remove(heroBotContext.Guilds.Where(x => x.DiscordId == e.Id).First());
                return heroBotContext.SaveChangesAsync();
            };
        }

        public Task MessageReceviedEventArgs(SocketMessage messageReceviedEventArgs)
        {
            var guild = messageReceviedEventArgs.Channel as SocketGuildChannel;
            if (guild != null && !messageReceviedEventArgs.Author.IsBot && !messageReceviedEventArgs.Author.IsWebhook)
                AddExpToUser(messageReceviedEventArgs.Author, guild.Guild, messageReceviedEventArgs.Content, messageReceviedEventArgs.Channel);
            return Task.CompletedTask;
        }
        /// <summary>
        /// The OnEvent
        /// </summary>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns

        private async Task AddExpToUser(SocketUser author, SocketGuild guild, String message, ISocketMessageChannel channel)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var config = await heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.guildConfig).Include(x => x.GuildUsers).FirstAsync();
                if (config.guildConfig.Levels)
                {
                    var exp = (message.Length > 20 ? 20 : message.Length);
                    if (config.GuildUsers.Where(x => x.DiscordId == author.Id).Count() > 0)
                    {

                        var userD = config.GuildUsers.Where(x => x.DiscordId == author.Id).First();
                        if (userD.LastExpGain < DateTime.Now.AddSeconds(-20))
                        {
                            userD.Messages = (userD.Messages + 1);
                            userD.Exp += exp;
                            var levelUpMessage = config.guildConfig.ExpMessage;
                            var levelUp = false;
                            while (userD.Exp >= CalculteNeedExp(userD.Level, config.guildConfig.Difficulty))
                            {
                                userD.Level++;
                                levelUp = true;
                            }
                            if (levelUp)
                            {
                                var msg = channel.SendMessageAsync(levelUpMessage.Replace("{user}", author.Mention).Replace("{level}", (userD.Level).ToString()).Replace("{expNextLevel}", CalculteNeedExp(userD.Level, config.guildConfig.Difficulty).ToString()));

                            }
                            userD.LastExpGain = DateTime.Now;
                            heroBotContext.Update(userD);
                            await heroBotContext.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        await CreateUser(author, guild, exp);
                    }

                }
            
            };
        }

        public long CalculteNeedExp(long level, int difficulty)
        {
            try
            {
                var r = 0L;
                r = Convert.ToInt64(level * Math.Pow(2, Math.Log(level * (difficulty * 100))));

                return r;
            }
            catch (Exception e) { Console.WriteLine(e); }
            return 0;
        }

        private async Task CreateUser(SocketUser user, SocketGuild guild, int exp, bool left = false, List<Warn> warns = null)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                if (heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Count() > 0)
                {
                    var guildData = await heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.GuildUsers).FirstAsync();
                    if (!guildData.GuildUsers.Exists(x => x.DiscordId == user.Id))
                    {
                        var userD = new GuildUser()
                        {
                            DiscordId = user.Id,
                            Warns = warns == null ? new List<Warn>() : warns,
                            Messages = 0,
                            Exp = exp,
                            Level = 1,
                            LastExpGain = DateTime.Now.AddDays(-1),
                            Joins = 1,
                            Present = !left
                        };
                        guildData.GuildUsers.Add(userD);
                        heroBotContext.Add(userD);
                        await heroBotContext.SaveChangesAsync();
                    }
                }
            };
        }

        /*[CommandsService.Command("config", permission = new[] { Discord.GuildPermission.Administrator }, permissionsNeed = new[] { Discord.GuildPermission.Administrator }, isGuildOnly = true)]
        public void EnableLevels(MessageReceviedEventArgs _eventArgs, [Ignore]string action, [Ignore]string key = null, [Ignore]string value = null)
        {
            var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
            using (var database = new LiteDatabase($"data/Guild-{guild.Id}.db"))
            {
                var config = database.GetCollection("config");
                EmbedBuilder embed = new EmbedBuilder();
                if (action == "list")
                {
                    StringBuilder str = new StringBuilder();
                    config.FindAll().ToList().ForEach((x) =>
                    {
                        var s = x["value"].AsString;
                        if (x["ConfigType"].AsString == ConfigType.BOOLEAN.ToString())
                        {
                            s = x["value"].AsBoolean ? "Oui" : "Non";
                        }
                        else if (x["ConfigType"].AsString == ConfigType.CHANNEL.ToString())
                        {
                            s = "<#" + s + ">";
                        }
                        else if (x["ConfigType"].AsString == ConfigType.ROLE.ToString())
                        {
                            s = "<@&" + s + ">";
                        }
                        else if (x["ConfigType"].AsString == ConfigType.USER.ToString())
                        {
                            s = "<@" + s + ">";
                        }
                        else
                        {
                            s = "*`" + s + "`*";
                        }
                        str.Append(" - **`").Append(x["name"].AsString).Append("` -->** ").Append(s).Append("\r");
                    });
                    _eventArgs.Message.Channel.SendMessageAsync(str.ToString());
                }
                else if (action == "get")
                {
                    if (key != null)
                    {
                        var result = config.FindAll().Where(x => x["name"] == key).ToList();
                        if (result.Count == 1)
                        {
                            StringBuilder str = new StringBuilder();
                            result.ForEach((x) =>
                            {
                                var s = x["value"].AsString;
                                if (x["ConfigType"].AsString == ConfigType.BOOLEAN.ToString())
                                {
                                    s = x["value"].AsBoolean ? "Oui" : "Non";
                                }
                                else if (x["ConfigType"].AsString == ConfigType.CHANNEL.ToString())
                                {
                                    s = "<#" + s + ">";
                                }
                                else if (x["ConfigType"].AsString == ConfigType.ROLE.ToString())
                                {
                                    s = "<@&" + s + ">";
                                }
                                else if (x["ConfigType"].AsString == ConfigType.USER.ToString())
                                {
                                    s = "<@" + s + ">";
                                }
                                else
                                {
                                    s = "*`" + s + "`*";
                                }
                                str.Append(" - **`").Append(x["name"].AsString).Append("` -->** ").Append(s).Append("\r");
                            });
                            _eventArgs.Message.Channel.SendMessageAsync(str.ToString());
                        }
                        else
                        {
                            _eventArgs.Message.Channel.SendMessageAsync($"La configuration {key} est introuvable !");
                        }
                    }
                    else
                    {
                        _eventArgs.Message.Channel.SendMessageAsync("Vous devez spécifier l'argument `key`");
                    }
                }
                else if (action == "set")
                {
                    if (key != null && value != null)
                    {
                        var result = config.FindAll().Where(x => x["name"] == key).ToList().FirstOrDefault();
                        if (result != null)
                        {
                            if (result["ConfigType"].AsString == ConfigType.BOOLEAN.ToString())
                            {
                                if (value == "on" || value == "true" || value == "enabled")
                                {
                                    result["value"] = true;
                                }
                                else if (value == "off" || value == "true" || value == "disabled")
                                {
                                    result["value"] = false;
                                }
                                config.Update(result);
                            }
                            else if (result["ConfigType"].AsString == ConfigType.CHANNEL.ToString())
                            {
                                foreach (var u in _eventArgs.Message.MentionedChannels)
                                {
                                    long v;
                                    if (long.TryParse(value.Replace("<#", string.Empty).Replace(">", string.Empty), out v))
                                    {
                                        result["value"] = v;
                                        config.Update(result);
                                    }
                                    else
                                    {
                                        _eventArgs.Message.Channel.SendMessageAsync($"Channel invalide");
                                    }
                                }
                            }
                            else if (result["ConfigType"].AsString == ConfigType.ROLE.ToString())
                            {
                                long v;
                                if (long.TryParse(value.Replace("<@&", string.Empty).Replace(">", string.Empty), out v))
                                {
                                    result["value"] = v;
                                    config.Update(result);
                                }
                                else
                                {
                                    _eventArgs.Message.Channel.SendMessageAsync($"Role invalide");
                                }
                            }
                        }
                        else
                        {
                            _eventArgs.Message.Channel.SendMessageAsync($"La configuration {key} est introuvable !");
                        }
                    }
                    else
                    {
                        _eventArgs.Message.Channel.SendMessageAsync("Vous devez spécifier l'argument `key` et `value`");
                    }
                }
                else
                {
                    _eventArgs.Message.Channel.SendMessageAsync("Vous ne pouvez faire que `;config [set|get|list]`");
                }


            }*/


        [CommandsService.Command("debuguser")]
        public async Task DebugUser(CommandContext _eventArgs, SocketUser socketUser = null)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
                var user = _eventArgs.Message.Author;
                if (socketUser != null)
                    user = socketUser;
                    GuildUser userD = null;
                var guildd = heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.guildConfig).Include(x => x.GuildUsers).First();
                    if (guildd.GuildUsers.Where(x => x.DiscordId == user.Id).Count() > 0)
                    {
                        userD = guildd.GuildUsers.Where(x => x.DiscordId == user.Id).First();
                    }
                    
                    guildd.GuildUsers = null;
                    userD.Guild = null;
                    guildd.guildConfig.Guild = null;
                    await _eventArgs.Message.Channel.SendMessageAsync("GuildUser details :"+(userD == null ? "```null```" : "```json\n"+JsonConvert.SerializeObject(userD).Take(20)+"```"));
                    //await _eventArgs.Message.Channel.SendMessageAsync("Guild details : ```json\n" + (JsonConvert.SerializeObject(guildd)).Take(20)+"```");
            }
        }

        [CommandsService.Command("level", isGuildOnly = true)]
        public async Task Level(CommandContext _eventArgs, SocketUser socketUser = null)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
                var user = _eventArgs.Message.Author;
                if (socketUser != null)
                    user = socketUser;

                var guildd = heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.guildConfig).Include(x => x.GuildUsers).First();
                if (guildd.guildConfig.Levels)
                {
                    if (guildd.GuildUsers.Where(x => x.DiscordId == user.Id).Count() > 0)
                    {
                        var userD = guildd.GuildUsers.Where(x => x.DiscordId == user.Id).FirstOrDefault();
                        await _eventArgs.Message.Channel.SendMessageAsync($"{user.Username} a { userD.Exp} d'exp et est de niveau " + userD.Level + " prochain niveau : \n```css\n" + makeBar(CalculteNeedExp(userD.Level, guildd.guildConfig.Difficulty), userD.Exp) + "```\n Il lui manque " + (CalculteNeedExp(userD.Level, guildd.guildConfig.Difficulty) - userD.Exp) + " d' exp pour passer au niveau supérieur!");
                    }
                    else
                    {
                        await _eventArgs.Message.Channel.SendMessageAsync($"{user.Mention} N'as jamais parlé !");
                    }
                }
                else
                    await _eventArgs.Message.Channel.SendMessageAsync("Le système de niveaux n'est pas activé ici");
            }
        }

        [CommandsService.Command("levelsconfig", isGuildOnly = true,permissionsNeed = new[] { GuildPermission.Administrator },permission = new[] { GuildPermission.Administrator })]
        public async Task LevelConfig(CommandContext _eventArgs, [Ignore]string action = null, [Ignore]string key = null, [Ignore][Remainer]string value = null)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
                var user = _eventArgs.Message.Author;
                var guildd = heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.GuildUsers).Include(x => x.guildConfig).First();
                Console.WriteLine($"{action} {key} {value}");
                if (action == "view")
                {
                    await _eventArgs.Message.Channel.SendMessageAsync($"\rLevelUpMessage: `{guildd.guildConfig.ExpMessage}`\nActivé: `{(guildd.guildConfig.Levels ? "Oui" : "Non")}`\nDifficultée: `{guildd.guildConfig.Difficulty}`");
                }
                else if (action == "set")
                {
                    if (value != null && key != null)
                    {
                        if (key == "message")
                        {

                            guildd.guildConfig.ExpMessage = value;
                            heroBotContext.Update(guildd.guildConfig);
                            heroBotContext.SaveChanges();
                            await _eventArgs.Message.Channel.SendMessageAsync($"Le nouveau messages est désormais : `{value}`");
                        }
                        else if (key == "activated")
                        {
                            if (value == "on" || value == "enabled" || value == "true")
                            {
                                guildd.guildConfig.Levels = true;
                                heroBotContext.Update(guildd.guildConfig);
                                heroBotContext.SaveChanges();
                                await _eventArgs.Message.Channel.SendMessageAsync($"Les système de niveaux de HeroBot est désormais **activé**");
                            }
                            else if (value == "off" || value == "disabled" || value == "false")
                            {
                                guildd.guildConfig.Levels = false;
                                heroBotContext.Update(guildd.guildConfig);
                                heroBotContext.SaveChanges();
                                await _eventArgs.Message.Channel.SendMessageAsync($"Les système de niveaux de HeroBot est désormais **désactivé**");
                            }
                            else
                            {
                                await _eventArgs.Message.Channel.SendMessageAsync($"Je n'ai pas compris ...");
                            }
                        }
                        else if (key == "difficulty")
                        {
                            int diff;
                            if (int.TryParse(value, out diff))
                            {
                                if (diff > 20 || diff < 0)
                                {
                                    await _eventArgs.Message.Channel.SendMessageAsync($"La difficultée doit etre domprise entre 0 et 20 !");

                                }
                                else
                                {
                                    guildd.guildConfig.Difficulty = diff;
                                    if (guild.MemberCount > 100)
                                    {
                                        await _eventArgs.Message.Channel.SendMessageAsync($"Ca vas prendre quelques temps !");
                                    }
                                    guildd.GuildUsers.ForEach(x =>
                                    {
                                        x.Level = 0;
                                        heroBotContext.Update(guildd);
                                    });
                                    heroBotContext.Update(guildd.guildConfig);
                                    heroBotContext.SaveChanges();
                                    await _eventArgs.Message.Channel.SendMessageAsync($"La difficultée des niveau est désormais a **{diff}**");
                                }
                            }
                            else
                            {
                                await _eventArgs.Message.Channel.SendMessageAsync($"Je n'ai pas compris ...");
                            }
                        }
                    }
                }
            }
        }
        [CommandsService.Command("resetexp", permissionsNeed = new[] { GuildPermission.Administrator }, permission = new[] { GuildPermission.Administrator }, isGuildOnly = true)]
        public async Task ResetExp(CommandContext _eventArgs)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
                var user = _eventArgs.Message.Author;
                var guildd = heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.GuildUsers).Include(x => x.guildConfig).First();

                if (guildd.guildConfig.Levels)
                {

                    if (guild.MemberCount > 100)
                    {
                        await _eventArgs.Message.Channel.SendMessageAsync($"Ca vas prendre quelques temps !");
                    }

                    guildd.GuildUsers.ForEach(x =>
                    {
                        x.Level = 0;
                        x.Exp = 0;
                        heroBotContext.Update(guildd);
                    });
                    await heroBotContext.SaveChangesAsync();
                    await _eventArgs.Message.Channel.SendMessageAsync("Le niveau de ce serveur a été reset !");
                }
                else
                    await _eventArgs.Message.Channel.SendMessageAsync("Le système de niveaux n'est pas activé ici");
            }
        }

        [CommandsService.Command("leaderboard")]
        [CommandsService.Command("lb")]
        public async Task LeaderBoard(CommandContext _eventArgs)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;
                var guildd = await heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.guildConfig).Include(x => x.GuildUsers).FirstAsync();
                if (guildd.guildConfig.Levels)
                {
                    var topexp = guildd.GuildUsers.Where(x => x.Present).OrderByDescending(x => x.Exp).Take(10);
                    var toplevel = guildd.GuildUsers.Where(x => x.Present).OrderByDescending(x => x.Level).ThenByDescending(x => x.Exp).Take(10);
                    var embed = new EmbedBuilder();
                    int i = 0;
                    var stringBuilder = new StringBuilder();
                    topexp.ToList().ForEach(x =>
                    {
                        i++;
                        string s = null;
                        if (i == 1)
                        {
                            s = "`🥇`1";
                        }
                        else if (i == 2)
                        {
                            s = "`🥈`2";
                        }
                        else if (i == 3)
                        {
                            s = "`🥉`3";
                        }
                        stringBuilder.Append("\n**").Append(s == null ? "#" + i : s).Append("** - <@!").Append(x.DiscordId).Append("> - `").Append(x.Exp).Append("`XP");
                    });
                    embed.AddField("Top EXP :", stringBuilder.ToString());
                    stringBuilder = new StringBuilder();
                    i = 0;
                    toplevel.ToList().ForEach(x =>
                    {
                        i++;
                        string s = null;
                        if (i == 1)
                        {
                            s = "`🥇`1";
                        }
                        else if (i == 2)
                        {
                            s = "`🥈`2";
                        }
                        else if (i == 3)
                        {
                            s = "`🥉`3";
                        }
                        stringBuilder.Append("\n**").Append(s == null ? "#" + i : s).Append("** - <@!").Append(x.DiscordId).Append("> - `LVL").Append(x.Level).Append("`");
                    });
                    embed.AddField("Top LEVEL :", stringBuilder.ToString());
                    embed.WithTitle("LEADERBOARD - " + guild.Name);
                    Random r = new Random();
                    embed.WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
                    await _eventArgs.Message.Channel.SendMessageAsync(embed: embed.Build());
                }
                else
                {
                    await _eventArgs.Message.Channel.SendMessageAsync("Le système de niveaux n'est pas activé ici");
                }
            };
        }
        internal string makeBar(long longeurChanson, long
            sec)
        {
            string s = "";
            if (longeurChanson < sec)
            {
                return "Error";
            }

            string r = " > " + sec + "/" + longeurChanson;
            long longeurBarre = 50 - (r.Length + 3);
            long caraceresAvant = Convert.ToInt64((sec * longeurBarre) / longeurChanson);
            long caracteresAppres = longeurBarre - caraceresAvant;
            s += "[";
            for (int i = 0; caraceresAvant != i; i++)
            {
                s += "=";
            }
            s += ">";
            for (int i = 0; caracteresAppres != i; i++)
            {
                s += "=";
            }
            s += "]" + r;
            return s;
        }
        public async Task CreateGuild(SocketGuild guild)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                if (heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Take(1).Count() < 1)
                {
                    var guildconfig = new GuildConfig()
                    {
                        ExpMessage = ":tada: | Félicitations {user} ! Tu viens de passer au niveau {level}",
                        WelcomeMessage = "Hey bienvenu {user} ! Passe un bon moment sur `" + guild.Name + "`",
                        Levels = false,
                        WelcomeChannel = guild.DefaultChannel.Id,
                        Difficulty = 4
                    };
                    var guildUsers = new List<GuildUser>();
                    Console.WriteLine("Registering guild : " + guild.Name);
                    var guildd = heroBotContext.Add(guildconfig);
                    var e = new Guild()
                    {
                        DiscordId = guild.Id,
                        Reputation = 0,
                        guildConfig = guildd.Entity,
                        Language = "en",
                        GuildUsers = guildUsers,
                    };
                    heroBotContext.Add(e);
                    await heroBotContext.SaveChangesAsync();
                    Console.WriteLine("Persisted guild data !");
                }
            };
        }
        [CommandsService.Command("warn", isGuildOnly = true, permissionsNeed = new[] { GuildPermission.ManageMessages }, permission = new[] { GuildPermission.ManageMessages })]
        public async Task WarnUser(CommandContext _eventArgs, SocketUser target, [Ignore]String reason)
        {
            SocketGuildChannel channel = _eventArgs.Message.Channel as SocketGuildChannel;
            using (HeroBotContext context = new HeroBotContext(DbContextOptions))
            {
                try
                {
                    var guildd = await context.Guilds.Where(x => x.DiscordId == channel.Guild.Id).Include(x => x.guildConfig).Include(x => x.GuildUsers).FirstAsync();
                    if (guildd.GuildUsers.Where(x => x.DiscordId == target.Id).Count() > 0)
                    {
                        var userD = guildd.GuildUsers.Where(x => x.DiscordId == target.Id).FirstOrDefault();
                        userD = context.GuildUsers.Where(x => x.GuildUserId == userD.GuildUserId).Include(x => x.Warns).First();
                        var v = new Warn()
                        {
                            Reason = reason,
                            Author = _eventArgs.Message.Author.Id
                        };
                        userD.Warns.Add(v);
                        context.Add(v);
                        context.Update(userD);
                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        await CreateUser(target, channel.Guild, 0, false, new List<Warn>() {
                        new Warn(){
                            Reason = reason,
                            Author = _eventArgs.Message.Author.Id
                        }
                        });
                    }
                    await _eventArgs.Message.Channel.SendMessageAsync($"<:Yes:530263286952886292> | L'utilisateur {target.Mention} a bien été warn pour `{reason}`");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            };
        }
        [CommandsService.Command("infractions", permission = new[] { GuildPermission.ManageMessages }, permissionsNeed = new[] { GuildPermission.ManageChannels })]
        public async Task InfractionsCommand(CommandContext _eventArgs, SocketUser target)
        {
            using (HeroBotContext heroBotContext = new HeroBotContext(DbContextOptions))
            {
                var guild = ((SocketTextChannel)_eventArgs.Message.Channel).Guild;

                var guildd = heroBotContext.Guilds.Where(x => x.DiscordId == guild.Id).Include(x => x.GuildUsers).First();

                if (guildd.GuildUsers.Where(x => x.DiscordId == target.Id).Count() > 0)
                {
                    var userD = guildd.GuildUsers.Where(x => x.DiscordId == target.Id).FirstOrDefault();
                    var warns = heroBotContext.Warns.Where(x => x.GuildUserId == userD.GuildUserId).Take(10);
                    var embed = new EmbedBuilder();
                    var r = new Random();
                    embed.WithTitle($"Infractions comises par {target.Username}").WithThumbnailUrl(target.GetAvatarUrl()).WithColor(new Color(r.Next() % 255, r.Next() % 255, r.Next() % 255));
                    var sting = new StringBuilder();
                    await warns.ForEachAsync((warn) =>
                    {
                        sting.Append($"**`{warn.Reason}`** par <@!{warn.Author}> le {warn.Date}\n");
                    });
                    embed.WithDescription(sting.ToString());
                    await _eventArgs.Message.Channel.SendMessageAsync(embed: embed.Build());
                }
                else
                {
                    await _eventArgs.Message.Channel.SendMessageAsync($"{target.Mention} n'as aucune infraction !");
                }
            };

        }
    }
}
