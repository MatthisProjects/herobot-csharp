﻿namespace HeroBot.basicServices.apiService.Entities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="GuildConfig" />
    /// </summary>
    public class GuildConfig
    {


        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GuildConfigId { get; set; }

        public int GuildId { get; set; }
        public Guild Guild { get; set; }

        /// <summary>
        /// Defines the _channels
        /// </summary>
        public ulong WelcomeChannel { get; set; }

        public int Difficulty { get; set; } = 4;

        public bool Levels { get; set; } = false;

        public string WelcomeMessage { get; set; }
        public string ExpMessage { get; set; }
    }
}
