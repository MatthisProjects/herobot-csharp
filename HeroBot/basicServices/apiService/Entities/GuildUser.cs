﻿using HeroBot_2.basicServices.apiService.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HeroBot.basicServices.apiService.Entities
{
    public class GuildUser
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GuildUserId { get; set; }

        public long Level { get; set; }

        public long Exp { get; set; }

        public int GuildId { get; set; }
        [ForeignKey("GuildId")]
        public Guild Guild { get; set; }

        public ulong DiscordId { get; set; }
        public int Messages { get; set; }
        public DateTime LastExpGain { get; set; }
        public int Joins { get; set; }

        public List<Warn> Warns { get; set; }
        public bool Present { get; internal set; }
    }
}
