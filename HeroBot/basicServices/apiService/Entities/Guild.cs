﻿namespace HeroBot.basicServices.apiService.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="Guild" />
    /// </summary>
    public class Guild
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GuildId { get; set; }
        /// <summary>
        /// Gets or sets the DiscordId
        /// </summary>
        public ulong DiscordId { get; set; }
        /// <summary>
        /// Gets or sets the Language
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// Gets or sets the Reputation
        /// </summary>
        public int Reputation { get; set; }
        /// <summary>
        /// Gets or sets the EnabledPlugins
        /// </summary>
        //public List<Plugin> EnabledPlugins { get; set; } = new List<Plugin>();

        /// <summary>
        /// Defines the guildConfig
        /// </summary>
        public GuildConfig guildConfig { get; set; }

        public List<GuildUser> GuildUsers { get; set; }
    }
}
