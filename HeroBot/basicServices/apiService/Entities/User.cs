﻿namespace HeroBot.basicServices.apiService.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Defines the <see cref="User" />
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the DiscordId
        /// </summary>
        public ulong DiscordId { get; set; }

        /// <summary>
        /// Gets or sets the Money
        /// </summary>
        public long Money { get; internal set; } = 50;

        /// <summary>
        /// Gets or sets the Reputation
        /// </summary>
        public long Reputation { get; set; } = 0;

        /// <summary>
        /// Gets or sets the Language
        /// </summary>
        public string Language { get; set; }
    }
}
