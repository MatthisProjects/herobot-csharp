﻿using HeroBot.basicServices.apiService.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HeroBot_2.basicServices.apiService.Entities
{
    public class Warn
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WarnId { get; set; }

        public int GuildUserId { get; set; }

        public ulong Author { get; set; }

        public string Reason { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
