﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HeroBot_2.Migrations
{
    public partial class Warnsidfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Author",
                table: "Warns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Warns",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Author",
                table: "Warns");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Warns");
        }
    }
}
