﻿namespace HeroBot
{
    using Figgle;
    using HeroBot.basicServices.apiService;
    using HeroBot.Runtime;
    using Sentry;
    using System;

    /// <summary>
    /// Defines the <see cref="Program" />
    /// </summary>
    internal class Program
    {





        /// <summary>
        /// The Main
        /// </summary>
        /// <param name="args">The args<see cref="string[]"/></param>
        internal static void Main(string[] args)
        {
            using (SentrySdk.Init("https://c2ccfe76bc984f56870648ccc48d974d@sentry.io/1384055"))
            {
                try
                {

                    Console.WriteLine("[System Luncher] Lunching herobot logger System !");
                    Console.WriteLine(FiggleFonts.Henry3d.Render("HeroBot"));
                    Console.ForegroundColor = ConsoleColor.White;
                    Logger.Log.Information("Loading libraries ... ");
                    Console.WriteLine("Instanciating Service Manager and Main System Service Class !");
                    new ServicesManager().Wait();
                }
                catch (Exception e)
                {
                    Logger.Log.Error("Failed to start HeroBot !!!", e);
                    Console.Write(e);
                    return;
                }


                var variable = "";
                const string constante = "";
                if (variable == constante) { } else { }

            }
        }
    }
}
