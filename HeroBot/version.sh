#!/bin/sh
DATE=`date '+%Y-%m-%d'`
VERSION=`cat versionFile`
echo "-------------------"
echo "Build version : ${CI_COMMIT_SHA}/${CI_COMMIT_TAG}"
echo "Version : ${VERSION}"
echo "Commit : ${CI_COMMIT_SHA}"
echo "Branch : ${CI_JOB_NAME}"
echo "Date: ${DATE}"
echo "-------------------"

sed -i 's/%date%/'"${DATE}"'/g' Versioning.cs
sed -i 's/%version%/'"${VERSION}"'/g' Versioning.cs
sed -i 's/%branch%/'"${CI_JOB_NAME}"'/g' Versioning.cs
sed -i 's/%commit%/'"${CI_COMMIT_SHA}"'/g' Versioning.cs
sed -i 's/%build%/'"${CI_COMMIT_SHA}\/${CI_COMMIT_TAG}"'/g' Versioning.cs