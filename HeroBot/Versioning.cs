﻿namespace HeroBot
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="Versioning" />
    /// </summary>
    internal class Versioning
    {
        /// <summary>
        /// Defines the Version
        /// </summary>
        public static String Version = "﻿%version%";

        /// <summary>
        /// Defines the Commit
        /// </summary>
        public static String Commit = "`%branch%/%commit%/%build%`";

        /// <summary>
        /// Defines the build
        /// </summary>
        public static String build = "%build%";

        /// <summary>
        /// Defines the date
        /// </summary>
        public static String date = "%date%";
    }
}
