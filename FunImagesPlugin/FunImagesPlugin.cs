﻿namespace FunImagesPlugin
{
    using HeroBot.Runtime.Entities;
    using System;

    /// <summary>
    /// Class for images plugin
    /// </summary>
    public class FunImagesPlugin : Plugin
    {
        /// <summary>
        /// Get the name of the plugin
        /// </summary>
        /// <returns>Name of the plugin</returns>
        public override string GetName()
        {
            return "Plugin d'images d'HeroBot";
        }

        /// <summary>
        /// Get the services for the plugin
        /// </summary>
        /// <returns>Services Array</returns>
        public override Type[] GetServices()
        {
            return new[] { typeof(ImagesServices) };
        }

        /// <summary>
        /// Return the description
        /// </summary>
        /// <returns>Description of the plugin</returns>
        public override string GetDescription()
        {
            return "Ce plugins permet le génération d'images via HeroBot";
        }
    }
}
