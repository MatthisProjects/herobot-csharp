﻿namespace FunImagesPlugin
{
    using ImageMagick;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ImageEffects" />
    /// </summary>
    internal class ImageEffects
    {
        /// <summary>
        /// Defines the settings
        /// </summary>
        internal static MagickReadSettings settings = new MagickReadSettings()
        {
            BackgroundColor = MagickColors.LightBlue,
            FillColor = MagickColors.Blue,
            Font = "HelveticaNeue",
            FontPointsize = 36,
            Width = 320
        };

        /// <summary>
        /// The Paint
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Paint(IMagickImage profil)
        {
            profil.OilPaint();
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Negate
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Negate(IMagickImage profil)
        {
            profil.Negate();
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Charcoal
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Charcoal(IMagickImage profil)
        {
            profil.Charcoal();
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Ange
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Ange(IMagickImage profil)
        {
            profil.BlueShift();
            return profil.ToByteArray();
        }

        /// <summary>
        /// The WTF
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> WTF(IMagickImage profil)
        {
            profil.BlackThreshold(new Percentage(90));
            return profil.ToByteArray();
        }

        /// <summary>
        /// The BLURWTF
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> BLURWTF(IMagickImage profil)
        {
            profil.RotationalBlur(50);
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Shape
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Shape(IMagickImage profil)
        {
            profil.BrightnessContrast(new Percentage(99), new Percentage(99));
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Pixelize
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Pixelize(IMagickImage profil)
        {
            Random r = new Random();
            profil.VirtualPixelMethod = VirtualPixelMethod.Random;
            profil.Resize(6, 6);
            profil.Magnify();
            profil.Resize(r.Next(250, 300), r.Next(250, 300));
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Wave
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Wave(IMagickImage profil)
        {
            profil.Wave(PixelInterpolateMethod.Spline, 50, 50);
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Implode
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Implode(IMagickImage profil)
        {
            profil.Implode(2.5, PixelInterpolateMethod.Spline);
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Explode
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Explode(IMagickImage profil)
        {
            profil.Swirl(360);
            return profil.ToByteArray();
        }

        /// <summary>
        /// The Welcome
        /// </summary>
        /// <param name="text">The text<see cref="string"/></param>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Welcome(string text, IMagickImage profil)
        {
            profil.Resize(360, 350);
            profil.Format = MagickFormat.Png;
            profil.Alpha(AlphaOption.Set);
            IMagickImage copy = profil.Clone();

            copy.Distort(DistortMethod.DePolar, 0);
            copy.VirtualPixelMethod = VirtualPixelMethod.HorizontalTile;
            copy.BackgroundColor = MagickColors.None;
            copy.Distort(DistortMethod.Polar, 0);

            profil.Compose = CompositeOperator.DstIn;
            profil.Composite(copy, CompositeOperator.CopyAlpha);
            using (IMagickImage back = new MagickImage("imagesPl/assets/welcome.png"))
            {
                back.Composite(profil, new PointD(135, 300), CompositeOperator.Over);

                using (MagickImage image = new MagickImage("caption:" + text,settings))
                {
                    back.Composite(image, new PointD(550, 550), CompositeOperator.Over);
                }

                return back.ToByteArray();
            }
        }

        /// <summary>
        /// The Baby
        /// </summary>
        /// <param name="profil">The profil<see cref="IMagickImage"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public async static Task<byte[]> Baby(IMagickImage profil)
        {
            profil.Resize(149, 149);
            profil.Format = MagickFormat.Png;
            profil.Alpha(AlphaOption.Set);
            IMagickImage copy = profil.Clone();

            copy.Distort(DistortMethod.DePolar, 0);
            copy.VirtualPixelMethod = VirtualPixelMethod.HorizontalTile;
            copy.BackgroundColor = MagickColors.None;
            copy.Distort(DistortMethod.Polar, 0);

            profil.Compose = CompositeOperator.DstIn;
            profil.Composite(copy, CompositeOperator.CopyAlpha);
            using (IMagickImage back = new MagickImage("imagesPl/assets/baby.png"))
            {
                back.Composite(profil, new PointD(90, 64), CompositeOperator.Over);
                return back.ToByteArray();
            }
        }
    }
}
