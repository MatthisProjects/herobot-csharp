﻿namespace FunImagesPlugin
{
    using Discord.WebSocket;
    using HeroBot.basicServices.apiService;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService;
    using HeroBot.Services.CommandsService.ArgsManager;
    using HeroBot_2.basicServices.commandsSystem;
    using ImageMagick;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Threading.Tasks;

    /// <summary>
    /// Service for Images
    /// </summary>
    public class ImagesServices : Service
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ImagesServices"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public ImagesServices(ServicesManager sm, Plugin plugin) :
            base(sm, plugin)
        {
        }


        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            GetServicesManager().GetService<CommandDictionary>().Register(this, GetPlugin());
        }

        /// <summary>
        /// The peinture
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("paint", description = "...")]
        public async void peinture(CommandContext _eventArgs)
        {
            await execute("Paint", _eventArgs);
        }

        /// <summary>
        /// The negate
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("negate", description = "Be negative")]
        public async void negate(CommandContext _eventArgs)
        {
            await execute("Negate", _eventArgs);
        }

        /// <summary>
        /// The charbon
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("charcoal", description = "Charcoal 🎶")]
        public async void charbon(CommandContext _eventArgs)
        {
            await execute("Charcoal", _eventArgs);
        }

        /// <summary>
        /// The ange
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("ange", description = "OwO")]
        public async void ange(CommandContext _eventArgs)
        {
            await execute("Ange", _eventArgs);
        }

        /// <summary>
        /// The wtf
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("WTF", description = "What da fuck is this !")]
        public async void wtf(CommandContext _eventArgs)
        {
            await execute("WTF", _eventArgs);
        }

        /// <summary>
        /// The bwtf
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("BLURWTF", description = "What is in this picture `🤔`?!")]
        public async void bwtf(CommandContext _eventArgs)
        {
            await execute("BLURWTF", _eventArgs);
        }

        /// <summary>
        /// The shap
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("shape", description = "Shape")]
        public async void shap(CommandContext _eventArgs)
        {
            await execute("Shape", _eventArgs);
        }

        /// <summary>
        /// The wel
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("welcome", description = "Says a hello to your friends !")]
        public async void wel(CommandContext _eventArgs,[Ignore]String message)
        {
            await execute("Welcome", _eventArgs, message);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_eventArgs"></param>
        /// <param name="message"></param>
        [Command("3d", description = "Text in 3d")]
        public async void troisdé(CommandContext _eventArgs, [Ignore] String message)
        {
            if (message.Length > 20)
            {
                await _eventArgs.Message.Channel.SendMessageAsync("Pas plus de 5 caractères !");
                return;
            }
            await _eventArgs.Message.Channel.SendMessageAsync("```" + Figgle.FiggleFonts.Larry3d.Render(message) + "```");
        }
        [Command("banner", description = "Build a banner")]
        public async void banner(CommandContext _eventArgs, [Ignore] String message)
        {
            if (message.Length > 20)
            {
                await _eventArgs.Message.Channel.SendMessageAsync("Pas plus de 10 caractères !");
                return;
            }
            await _eventArgs.Message.Channel.SendMessageAsync("```" + Figgle.FiggleFonts.Banner.Render(message) + "```");
        }
        /// <summary>
        /// The pixelize
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("pixelize", description = "... Who uses this")]
        public async void pixelize(CommandContext _eventArgs)
        {
            await execute("Pixelize", _eventArgs);
        }

        /// <summary>
        /// The baby
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("baby", description = "OwO look at this beautiful baby ¯\\_(ツ)_/¯")]
        public async void baby(CommandContext _eventArgs)
        {
            await execute("Baby", _eventArgs);
        }

        /// <summary>
        /// The explode
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("explode", description = "Explodes yout picture profile")]
        public async void explode(CommandContext _eventArgs)
        {
            await execute("Explode", _eventArgs);
        }

        /// <summary>
        /// The implode
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("implode", description = "Implodes your picture profile")]
        public async void implode(CommandContext _eventArgs)
        {
            await execute("Implode", _eventArgs);
        }

        /// <summary>
        /// The wave
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [Command("wave", description = "_-_-_-_-_-")]
        public async void wave(CommandContext _eventArgs)
        {
            await execute("Wave", _eventArgs);
        }
        /// <summary>
        /// The execute
        /// </summary>
        /// <param name="s">The s<see cref="string"/></param>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        private async Task execute(string s, CommandContext _eventArgs, string message = null)
        {
            Discord.Rest.RestUserMessage e = await _eventArgs.Message.Channel.SendMessageAsync(":ok_hand: Processing ...");
            SocketUser r = _eventArgs.Message.Author;
            if (_eventArgs.Message.MentionedUsers.Count > 0)
            {
                r = _eventArgs.Message.MentionedUsers.First();
            }
            var m = "";
            if (message != null)
            {
                m = message;
                foreach (SocketUser u in _eventArgs.Message.MentionedUsers)
                {
                    m = m.Replace(u.Mention, "@" + u.Username).Replace(u.Mention.Replace("!", ""), "@" + u.Username);
                }
                foreach (SocketChannel c in _eventArgs.Message.MentionedChannels)
                {
                    if (c.GetType().Equals(typeof(SocketTextChannel)))
                    {
                        m = m.Replace(((SocketTextChannel)c).Mention, "#" + ((SocketTextChannel)c).Name);
                    }
                }
                foreach (SocketRole c in _eventArgs.Message.MentionedRoles)
                {
                    m = m.Replace(c.Mention, "@" + c.Name);
                }
            }
            byte[] byteArray = await getUser(r, typeof(ImageEffects).GetMethod(s), m);
            Stream stream = new MemoryStream(byteArray);
            await _eventArgs.Message.Channel.SendFileAsync(stream, s + ".png", "Here it is ! " + r.Mention);
            stream.Close();
            await e.DeleteAsync();
        }

        /// <summary>
        /// The getUser
        /// </summary>
        /// <param name="user">The user<see cref="SocketUser"/></param>
        /// <param name="method">The method<see cref="MethodInfo"/></param>
        /// <param name="atext">The atext<see cref="string"/></param>
        /// <returns>The <see cref="Task{byte[]}"/></returns>
        public Task<byte[]> getUser(SocketUser user, MethodInfo method, string atext = "nothink")
        {
            using (WebClient w = new WebClient())
            {
                using (MagickImage profil = new MagickImage(w.DownloadData(user.GetAvatarUrl())))
                {
                    profil.Resize(800, 800);
                    return executeMethod(method, profil, atext);
                }
            }
        }

        /// <summary>
        /// The executeMethod
        /// </summary>
        /// <param name="m">The m<see cref="MethodInfo"/></param>
        /// <param name="profil">The profil<see cref="MagickImage"/></param>
        /// <param name="atext">The atext<see cref="string"/></param>
        /// <returns>The <see cref="Task{byte[]}"/></returns>
        private async Task<byte[]> executeMethod(MethodInfo m, MagickImage profil, string atext = "No text")
        {
            MagickImage fa = profil;
            try
            {
                return await (Task<byte[]>)m.Invoke(null, new object[] { fa });
            }
            catch (Exception e)
            {
                try
                {
                    return await (Task<byte[]>)m.Invoke(null, new object[] { atext, fa });
                }
                catch (Exception)
                {
                }
            }
            return File.ReadAllBytes("imagesPl/assets/error-image-generic.png");
        }
    }
}
