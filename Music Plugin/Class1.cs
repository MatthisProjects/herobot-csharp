﻿namespace Music_Plugin
{
    using HeroBot.Runtime.Entities;
    using System;

    /// <summary>
    /// Defines the <see cref="Class1" />
    /// </summary>
    public class Class1 : Plugin
    {
        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public override Type[] GetServices()
        {
            return new Type[] { typeof(GuildMusicService) };
        }

        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetName()
        {
            return "HeroMusic";
        }

        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetDescription()
        {
            return "Allows you to play music in voice channels";
        }
    }
}
