﻿namespace Music_Plugin
{
    using Discord;
    using Discord.WebSocket;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService;
    using HeroBot_2.basicServices.commandsSystem;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Victoria;
    using Victoria.Entities;

    /// <summary>
    /// Defines the <see cref="GuildMusicService" />
    /// </summary>
    internal class GuildMusicService : Service
    {
        /// <summary>
        /// Gets the Lavalink
        /// </summary>
        public static LavaShardClient LavaShardClient;

        /// <summary>
        /// Defines the LavaNode
        /// </summary>
        public static LavaRestClient LavaRestClient;

        /// <summary>
        /// Defines the config
        /// </summary>
        public static Configuration config = new Configuration()
        {
            Host = "lavalink",
            Port = 80,
            Password = "3568",
            LogSeverity = LogSeverity.Verbose,
            SelfDeaf = true,
            AutoDisconnect = true
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="GuildMusicService"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public GuildMusicService(ServicesManager sm, Plugin plugin) :
            base(sm, plugin)
        {
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }


        public override void PreEnable()
        {

        }
        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            
            GetServicesManager().GetService<CommandDictionary>().Register(this, GetPlugin());
            var discordclient = GetServicesManager().GetService<BotService>().GetDiscordClient();
            discordclient.ShardConnected += async (shard) => {
                if (discordclient.Shards.Where(x => x.ConnectionState == ConnectionState.Connected).Count() == discordclient.Shards.Count)
                {
                    await StartLavalinkEngine(discordclient);
                }
            };

        }

        /// <summary>
        /// The PlayCommand
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="search">The search<see cref="string"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "play",
                description = "!command-play-description",
                isGuildOnly = true)
        ]
        public async Task PlayCommand(CommandContext _e, [Ignore]string search)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            if (music != null)
            {
                SearchResult r;
                if (search.StartsWith("http"))
                {
                    r = await LavaRestClient.SearchTracksAsync(search);
                }
                else
                {
                    r = await LavaRestClient.SearchYouTubeAsync(search);
                }
                if (r.LoadType == LoadType.LoadFailed)
                {
                    await _e.Reply("command-play-error-load-music");
                }
                else if (r.LoadType == LoadType.NoMatches)
                {
                    await _e.Reply("command-play-error-not-found");
                }
                else if (r.LoadType == LoadType.PlaylistLoaded)
                {
                    await _e.Reply("command-play-loaded-playlist", new Dictionary<string, string>()
                    {
                        ["songs_count"] = r.Tracks.Count().ToString(),
                        ["playlist_name"] = r.PlaylistInfo.Name
                    });
                    foreach (LavaTrack track in r.Tracks)
                    {
                        await music.PlayAsync(track);
                    }
                }
                else if (r.LoadType == LoadType.SearchResult)
                {
                    String s = "";
                    int i = 1;
                    foreach (LavaTrack lavaTrack in r.Tracks)
                    {
                        s += "**" + i + "** - `" + lavaTrack.Author + " : " + lavaTrack.Title + "`\r";
                        i++;
                    }
                    await _e.Reply("command-play-select-song", new Dictionary<string, string>()
                    {
                        ["choises"] = s
                    });
                }
                else if (r.LoadType == LoadType.TrackLoaded)
                {
                    await _e.Reply("command-play-loaded-song", new Dictionary<string, string>()
                    {
                        ["music"] = r.Tracks.First().Author + " : " + r.Tracks.First().Title
                    });
                    await music.PlayAsync(r.Tracks.First());
                }
            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }

        /// <summary>
        /// The Join
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "join",
                description = "command-join-description",
                isGuildOnly = true)
        ]
        public async Task Join(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            SocketVoiceState? state =
                channel.Guild.GetUser(_e.Message.Author.Id).VoiceState;
            if (music == null)
            {
                if (state.HasValue)
                {
                    music = await LavaShardClient.ConnectAsync(state.Value.VoiceChannel, _e.Message.Channel as SocketTextChannel);
                    
                    await _e.Reply("command-join-connected-content",
                                new Dictionary<string, string>()
                                {
                                    ["channel"] = state.Value.VoiceChannel.Name
                                });
                }
                else
                {
                    await _e.Reply("command-join-no-voice-channel");
                }
            }
            else
                await _e.Reply("pl-music-global-already-conected", new Dictionary<string, string>()
                {
                    ["channel"] = music.VoiceChannel.Name
                });
        }

        /// <summary>
        /// The Stop
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "stop",
                description = "!command-stop-description",
                isGuildOnly = true)
        ]
        public async Task Stop(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            if (music != null)
            {
                await LavaShardClient.DisconnectAsync(music.VoiceChannel);
                await _e.Reply("command-stop-ok-content");
            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }

        /// <summary>
        /// The Pause
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "pause",
                description = "!command-pause-description",
                isGuildOnly = true)
        ]
        public async Task Pause(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            if (music != null)
            {
                if (!music.IsPaused)
                {
                    await music.PauseAsync();
                    await _e.Reply("command-pause-paused");
                }

            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }

        /// <summary>
        /// The Skip
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "skip",
                description = "!command-skip-description",
                isGuildOnly = true)
        ]
        public async Task Skip(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            if (music != null)
            {
                    await music.SkipAsync();
                    await _e.Reply("command-skip-skipped");
            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }

        /// <summary>
        /// The Resume
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [
            Command(
                "resume",
                description = "!command-resume-description",
                isGuildOnly = true)
        ]
        public async Task Resume(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);
            if (music != null)
            {
                if (music.IsPaused)
                    await music.PauseAsync();
                await _e.Reply("command-resume-content");
            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }
        /// <summary>
        /// The NowPlaying
        /// </summary>
        /// <param name="e">The e<see cref="MessageReceviedEventArgs"/></param>
        /// <returns>The <see cref="Task"/></returns>
        [Command("nowplaying", description = "Mais c'est quoi cette musique ???", isGuildOnly = true)]
        public async Task NowPlaying(CommandContext _e)
        {
            var channel = _e.Message.Channel as SocketGuildChannel;
            LavaPlayer music = LavaShardClient.GetPlayer(channel.Guild.Id);

            if (music != null)
            {
                LavaTrack song = music.CurrentTrack;
                EmbedBuilder embed = new EmbedBuilder();
                embed.AddField(":musical_note:  En cours : ", song.Title + " de " + song.Author, false);
                embed.AddField(":musical_note: Source", "[" + song.Title + "," + song.Title + "](" + song.Uri + ")", false);
                embed.AddField("Avancement", makeBar(song.Length, song.Position), false);
                embed.Color = Color.Blue;
                await _e.Message.Channel.SendMessageAsync(embed: embed.Build());
            }
            else
            {
                await _e.Reply("pl-music-global-not-connected");
            }
        }

        /// <summary>
        /// The makeBar
        /// </summary>
        /// <param name="longeurChanson">The longeurChanson<see cref="TimeSpan"/></param>
        /// <param name="sec">The sec<see cref="TimeSpan"/></param>
        /// <returns>The <see cref="string"/></returns>
        internal string makeBar(TimeSpan longeurChanson, TimeSpan sec)
        {
            string s = "";
            if (longeurChanson < sec)
            {
                return "Error";
            }
            TimeSpan time = sec;
            string str = time.ToString(@"hh\:mm\:ss");
            TimeSpan rd = longeurChanson;
            string rds = rd.ToString(@"hh\:mm\:ss");
            string r = " > " + str.Replace("00:", "") + "/" + rds.Replace("00:", "");
            long longeurBarre = 30 - (r.Length + 3);
            long caracèresAvant = Convert.ToInt64((sec * longeurBarre) / longeurChanson);
            long caractèresApprès = longeurBarre - caracèresAvant;
            s += "[";
            for (int i = 0; caracèresAvant != i; i++)
            {
                s += "=";
            }
            s += ">";
            for (int i = 0; caractèresApprès != i; i++)
            {
                s += "=";
            }
            s += "]" + r;
            return s;
        }
        public async Task StartLavalinkEngine(DiscordShardedClient e)
        {
            LavaShardClient = new LavaShardClient();
            await LavaShardClient.StartAsync(e,config);
            LavaShardClient.OnTrackStuck += OnTrackStuck;
            LavaShardClient.OnTrackFinished += OnTrackFinished;
            LavaShardClient.OnTrackException += OnTrackException;
            LavaShardClient.ToggleAutoDisconnect();
        }

        /// <summary>
        /// The OnTrackStuck
        /// </summary>
        /// <param name="arg1">The arg1<see cref="LavaPlayer"/></param>
        /// <param name="arg2">The arg2<see cref="LavaTrack"/></param>
        /// <param name="arg3">The arg3<see cref="long"/></param>
        /// <returns>The <see cref="Task"/></returns>
        private Task OnTrackStuck(LavaPlayer arg1, LavaTrack arg2, long arg3)
        {
            return arg1.TextChannel.SendMessageAsync("Je n'est pas pu atteindre la musique " + "`" + arg2.Author + " : " + arg2.Title + "` j'ai essayé pendant " + arg3 + " secondes");
        }

        /// <summary>
        /// The OnTrackFinished
        /// </summary>
        /// <param name="arg1">The arg1<see cref="LavaPlayer"/></param>
        /// <param name="arg2">The arg2<see cref="LavaTrack"/></param>
        /// <param name="arg3">The arg3<see cref="TrackReason"/></param>
        /// <returns>The <see cref="Task"/></returns>
        private Task OnTrackFinished(LavaPlayer arg1, LavaTrack arg2, TrackEndReason arg3)
        {
            arg1.PlayAsync(arg2, true);
            return arg1.TextChannel.SendMessageAsync("Prochainne chanson : `" + arg2.Author + " : " + arg2.Title + "`");
        }

        /// <summary>
        /// The OnTrackException
        /// </summary>
        /// <param name="arg1">The arg1<see cref="LavaPlayer"/></param>
        /// <param name="arg2">The arg2<see cref="LavaTrack"/></param>
        /// <param name="arg3">The arg3<see cref="string"/></param>
        /// <returns>The <see cref="Task"/></returns>
        private Task OnTrackException(LavaPlayer arg1, LavaTrack arg2, string arg3)
        {
            return arg1.TextChannel.SendMessageAsync("Une erreur est survenue durant le chanson :/ `" + arg2.Author + " : " + arg2.Title + "`");
        }

    }
}
