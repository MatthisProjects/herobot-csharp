## **HeroBot bot discord Pluggable !**
> Bienvenu sur la page-projet de HeroBot !
[Visit our **Super WebSite !!! **](https://alivecreation.fr)
Le projet est divis en plusieurs parties, 
  * [HeroBot](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/HeroBot) `./HeroBot`
  * [Music Plugin](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/Music%20Plugin) `./Music Plugin`
  * [Fun Images Plugin](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/FunImagesPlugin) `./FunImagesPlugin`
  * [Utilities](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/Utilities) `./Utilities`
  * [_VCS Plugin_](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/VCSPlugin) `./VCSPlugin`
  * [GitLabIntegration](https://gitlab.com/MatthisProjects/herobot-csharp/tree/master/GitLabIntegration) `./GitLabIntegration`

| **Master** 	| ![GitLabCI](https://gitlab.com/MatthisProjects/herobot-csharp/badges/master/pipeline.svg "GitLabCI Pipeline") 	|
|-------------	|----------------------------------------------------------------------------------------------------------------	|
| **Nightly** 	| ![GitLabCI](https://gitlab.com/MatthisProjects/herobot-csharp/badges/nightly/pipeline.svg "GitLabCI Pipeline") 	|


[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2FMatthisProjects%2Fherobot-csharp.svg?type=large)](https://app.fossa.io/projects/git%2Bgitlab.com%2FMatthisProjects%2Fherobot-csharp?ref=badge_large)