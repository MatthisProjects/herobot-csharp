﻿namespace DevTools
{
    using Discord;
    using Discord.Rest;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService;
    using HeroBot_2.basicServices.commandsSystem;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the DevTools's Agent<see cref="Agent" />
    /// </summary>
    internal class Agent : Service
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="Agent"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public Agent(ServicesManager sm, Plugin plugin) : base(sm, plugin)
        {
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
            
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            GetServicesManager().GetService<CommandDictionary>().Register(this, GetPlugin());
        }


        [Command("checkCode")]
        public async Task oncheckCode(CommandContext _messageReceviedEventArgs)
        {
            var code = _messageReceviedEventArgs.Message.Content.ToLower().Replace(";checkcode", String.Empty);
            if ((code.StartsWith("https://") || code.StartsWith("http://")) && code.Contains("pastebin.com"))
            {
                var url = code;
                var pCode = url.Replace("https://", String.Empty).Split("/")[1];
                using (WebClient c = new WebClient())
                {
                    c.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36");
                    var bytes = c.DownloadData(new Uri("https://pastebin.com/raw/" + pCode));
                    var s = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                    if (s.Equals("Error with this ID!")) await _messageReceviedEventArgs.Message.Channel.SendMessageAsync("Je n'ai pas pu récuperer le pastebin :/");
                    await _messageReceviedEventArgs.Message.Channel.SendMessageAsync(await Check(s));
                }
            }
            await _messageReceviedEventArgs.Message.Channel.SendMessageAsync(await Check(code.Replace("```", String.Empty)));
        }
        /// <summary>
        /// The Check
        /// </summary>
        /// <param name="code">The code<see cref="String"/></param>
        /// <returns>Message to send<see cref="Task{String}"/></returns>
        public async Task<String> Check(String code)
        {
            using (HttpClient c = new HttpClient())
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(new Uri("http://jshint:3000"));
                webRequest.Method = "GET";
                code = $"'use strict';\n/* jshint -W097 */\n/* jshint node: true */\n/*jshint esversion: 6 */\n{code}";
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] byte1 = encoding.GetBytes(code);

                // Set the content length of the string being posted.
                webRequest.ContentLength = byte1.Length;
                webRequest.Method = "POST";
                Stream newStream = webRequest.GetRequestStream();

                newStream.Write(byte1, 0, byte1.Length);
                WebResponse g = await webRequest.GetResponseAsync();
                String responseString = "";
                using (Stream stream = g.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                }

                var errors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Error>>(responseString);
                var result = "```diff\n+Analyse de code : ```\n";
                if (errors.Count == 0) result += "`Aucune erreur a été trouvée ;)`";
                int i = 0;
                foreach (Error err in errors)
                {
                    var enp = "`" + err.code + " position:" + err.line + ":" + err.character + ", " + err.reason + "`\n";
                    if (result.Length + enp.Length >= 1990)
                    {
                        result += "And " + (errors.Count - i) + " others";
                        break;
                    }
                    result += enp;
                    i++;
                }
                return result;
            }
        }
    }

    /// <summary>
    /// Defines the <see cref="Error" />
    /// </summary>
    internal class Error
    {
        /// <summary>
        /// Defines the code of the error
        /// </summary>
        public string code;

        /// <summary>
        /// Defines the line of the error
        /// </summary>
        public string line;

        /// <summary>
        /// Defines the character of the error
        /// </summary>
        public string character;

        /// <summary>
        /// Defines the reason of the error
        /// </summary>
        public string reason;
    }
}
