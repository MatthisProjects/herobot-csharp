﻿namespace DevTools
{
    using HeroBot.Runtime.Entities;
    using System;

    /// <summary>
    /// Defines the <see cref="DevToolsPlugin" />
    /// </summary>
    public class DevToolsPlugin : Plugin
    {
        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetDescription()
        {
            return "Outils et truc utiles pour développeurs";
        }

        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetName()
        {
            return "Developpers tools";
        }

        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public override Type[] GetServices()
        {
            return new[] { typeof(Agent) };
        }
    }
}
