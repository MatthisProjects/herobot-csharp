FROM mcr.microsoft.com/dotnet/core/runtime:3.0
WORKDIR /app
ADD fonts/* fonts/
RUN apt-get update && apt-get install -y \
    fontconfig && rm -rf /var/lib/apt/lists/*
RUN ls fonts
RUN cp fonts/* /usr/share/fonts
RUN mkdir data
RUN fc-cache -f -v
ADD ./out/ /app
ENTRYPOINT ["dotnet","HeroBot.dll"]