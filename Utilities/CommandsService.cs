﻿namespace HeroBot.Plugins.Utilities
{
    using Discord;
    using Discord.Rest;
    using Discord.WebSocket;
    using HeroBot.basicServices.commandsSystem.argsManager;
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;
    using HeroBot.Runtime.Entities.Traduction;
    using HeroBot.Services.BotManagerService;
    using HeroBot.Services.CommandsService;
    using HeroBot.Services.CommandsService.ArgsManager;
    using HeroBot_2.basicServices.commandsSystem;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="CommandsService" />
    /// </summary>
    internal class CommandsService : Service
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="CommandsService"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public CommandsService(ServicesManager sm, Plugin plugin) :
            base(sm, plugin)
        {
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
            GetServicesManager().GetService<CommandDictionary>().Register(this, GetPlugin());
        }

        // TODO ADD ID KICK
        /// <summary>
        /// The OnKickCommand
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="target">The target<see cref="SocketUser"/></param>
        /// <param name="reason">The reason<see cref="string"/></param>
        [
            Command(
                "kick",
                permission = new[] { GuildPermission.KickMembers },
                description = "!c-kick-description",
                isGuildOnly = true,
                permissionsNeed = new[] { GuildPermission.KickMembers })
        ]
        public async void OnKickCommand(
            CommandContext _eventArgs, SocketUser target, [Remainer][Ignore]string reason = "No reason"
        )
        {
            SocketGuildUser member = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            if (member.GuildPermissions.Has(GuildPermission.ManageMessages) || member.Hierarchy >= g.CurrentUser.Hierarchy || member.Id.Equals(g.OwnerId) || member.Id.Equals(_eventArgs.Message.Author.Id))
            {
                await _eventArgs.Reply("c-kick-cant-kick-moderator", new Dictionary<string, string>()
                {
                    ["user"] = target.Mention
                });
                return;
            }
            await member.SendMessageAsync("c-kick-kicked-message".Traduct(new Dictionary<string, string>()
            {
                ["mention"] = member.Mention,
                ["server"] = member.Guild.Name,
                ["kicker"] = _eventArgs.Message.Author.Username,
                ["raison"] = reason,
                ["link"] = (await g.DefaultChannel.CreateInviteAsync(maxUses: 1)).Url
            }, g, member)).ConfigureAwait(false);
            await member.KickAsync("**Kick** : `" + reason + "` by " + _eventArgs.Message.Author.Username);
            await _eventArgs.Reply("c-kick-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator,
                ["reason"] = reason
            });
        }

        /// <summary>
        /// The OnBanCommand
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="target">The target<see cref="SocketUser"/></param>
        /// <param name="reason">The reason<see cref="string"/></param>
        [
            Command(
                "ban",
                permission = new[] { GuildPermission.BanMembers },
                description = "!c-ban-description",
                isGuildOnly = true,
                permissionsNeed = new[] { GuildPermission.BanMembers })
        ]
        public async void OnBanCommand(
             CommandContext _eventArgs, SocketUser target, [Remainer][Ignore]string reason = "No reason"
        )
        {
            SocketGuildUser member =
                ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            if (member.GuildPermissions.Has(GuildPermission.ManageMessages) || member.Hierarchy >= g.CurrentUser.Hierarchy || member.Id.Equals(g.OwnerId) || member.Id.Equals(_eventArgs.Message.Author.Id))
            {
                await _eventArgs.Reply("c-ban-cant-ban-moderator", new Dictionary<string, string>()
                {
                    ["user"] = target.Mention
                });
                return;
            }

            await
                member
                .SendMessageAsync("c-ban-banned-message".Traduct(new Dictionary<string, string>()
                {
                    ["mention"] = member.Mention,
                    ["server"] = member.Guild.Name,
                    ["kicker"] = _eventArgs.Message.Author.Username,
                    ["raison"] = reason
                }, g, member)).ConfigureAwait(false);
            await member.BanAsync(7, "**Ban** : " + reason + " by " + _eventArgs.Message.Author.Username);
            await _eventArgs.Reply("c-ban-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator,
                ["reason"] = reason
            });
        }

        /// <summary>
        /// The OnSoftBanCommand
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="target">The target<see cref="SocketUser"/></param>
        /// <param name="reason">The reason<see cref="string"/></param>
        [
            Command(
                "softBan",
                permission = new[] { GuildPermission.KickMembers },
                description = "!c-softban-description",
                isGuildOnly = true,
                permissionsNeed = new[] { GuildPermission.KickMembers })
        ]
        public async void OnSoftBanCommand(
             CommandContext _eventArgs, SocketUser target, [Remainer][Ignore]string reason = "No reason")
        {
            SocketGuildUser member =
                            ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            if (member.GuildPermissions.Has(GuildPermission.ManageMessages) || member.Hierarchy >= g.CurrentUser.Hierarchy || member.Id.Equals(g.OwnerId) || member.Id.Equals(_eventArgs.Message.Author.Id))
            {
                await _eventArgs.Reply("c-softban-cant-softban-moderator", new Dictionary<string, string>()
                {
                    ["user"] = member.Mention
                });
                return;
            }

            await
                member
                .SendMessageAsync("c-softban-softbanned-message".Traduct(new Dictionary<string, string>()
                {
                    ["mention"] = member.Mention,
                    ["server"] = member.Guild.Name,
                    ["kicker"] = _eventArgs.Message.Author.Username,
                    ["raison"] = reason,
                    ["link"] = (await g.DefaultChannel.CreateInviteAsync(maxUses: 1)).Url
                }, g, member)).ConfigureAwait(false);
            await member.BanAsync(7, "**SoftBan** : " + reason + " by " + _eventArgs.Message.Author.Username);
            await member.Guild.RemoveBanAsync(member);
            await _eventArgs.Reply("c-softban-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator,
                ["reason"] = reason
            });
        }

        /// <summary>
        /// The onClear
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        /// <param name="count">The count<see cref="int"/></param>
        [
            Command(
                "clear",
                permission = new[] { GuildPermission.ManageChannels },
                description = "!c-clear-description",
                permissionsNeed =
                new[] { GuildPermission.ManageMessages, GuildPermission.ManageChannels })
        ]
        public async void onClear(CommandContext _eventArgs, int count)
        {
            if (count > 99)
            {
                count = 99;
            }
            var e =
                _eventArgs.Message.Channel.GetMessagesAsync(count);
            if (await e.Count() < count)
            {
                count = await e.Flatten().Count();
            }
            await ((SocketTextChannel)_eventArgs.Message.Channel).DeleteMessagesAsync((e.Flatten().ToEnumerable()));
            RestMessage id =
                await _eventArgs.Reply("c-clear-confirmed", new Dictionary<string, string>()
                {
                    ["nombre_messages"] = (count).ToString() + " messages(s) "
                });
            new Task(() =>
            {
                Thread.Sleep(5000);
                id.DeleteAsync();

            }).Start();
        }
/*
        /// <summary>
        /// The onCreateNews
        /// </summary>
        /// <param name="_eventArgs">The _eventArgs<see cref="MessageReceviedEventArgs"/></param>
        [
            Command(
                "createNews",
                permission = new[] { GuildPermission.SendMessages, GuildPermission.ManageChannels },
                description = "!c-createnews-description",
                isGuildOnly = true,
                permissionsNeed =
                new[] { GuildPermission.Administrator })
        ]
        public void onCreateNews(CommandContext _eventArgs)
        {
            _eventArgs.Reply("c-createnews-state-1");
            ulong authorId = _eventArgs.Message.Author.Id;
            ulong channelId = _eventArgs.Message.Channel.Id;
            List<ulong> lasMessageId = new List<ulong>() { _eventArgs.Message.Id };
            ulong gid = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.Id;
            string msg = "";
            string title = "";
            Func<EventArgs, bool> verifyFunction =
                (e) =>
                {
                    if (e.GetType().Equals(typeof(MessageReceviedEventArgs)))
                    {
                        if (
                            ((MessageReceviedEventArgs)e).Message.Author.Id
                            .Equals(authorId) &&
                            ((MessageReceviedEventArgs)e).Message.Channel.Id
                            .Equals(channelId) &&
                            !lasMessageId.Contains(
                            ((MessageReceviedEventArgs)e).Message.Id)
                        )
                        {
                            return true;
                        }
                    }

                    return false;
                };
            w.Register(verifyFunction,
            (e) =>
            {
                MessageReceviedEventArgs args = (MessageReceviedEventArgs)e;
                title = args.Message.Content;
                args.Reply("c-createnews-state-2");
                lasMessageId.Add(args.Message.Id);
                w.Register(verifyFunction,
                (e2) =>
                {
                    MessageReceviedEventArgs args2 = (MessageReceviedEventArgs)e2;
                    msg = args2.Message.Content;
                    args2.Reply("c-createnews-state-3");
                    lasMessageId.Add(args.Message.Id);
                    w.Register(verifyFunction,
                    (e3) =>
                    {
                        MessageReceviedEventArgs args3 =
                            (MessageReceviedEventArgs)e3;
                        if (args3.Message.MentionedChannels.Count < 1)
                        {
                            return;
                        }
                        SocketTextChannel c = (SocketTextChannel)args3.Message.MentionedChannels.First();
                        if (!c.Guild.Id.Equals(gid))
                        {
                            args3.Reply("c-createnews-state-error");
                            return;
                        }

                        EmbedBuilder emb = new EmbedBuilder();
                        EmbedAuthorBuilder a =
                            new EmbedAuthorBuilder();
                        string d = "";
                        foreach (SocketRole r in args3.Message.MentionedRoles)
                        {
                            d += r.Mention + " ";
                        }
                        if ((args3.Message.Content.Contains("everyone")) || args3.Message.Content.Contains("everyone"))
                        {
                            d += "@everyone";
                        }
                        else
                        if ((args3.Message.Content.Contains("here")) || args3.Message.Content.Contains("here"))
                        {
                            d += "@here";
                        }
                        a.Name = args.Message.Author.Username;
                        a.IconUrl = args.Message.Author.GetAvatarUrl();
                        emb.Title = title;
                        emb.Description = msg;
                        emb.WithColor(Color.LighterGrey);
                        emb.Author = a;
                        emb.WithTimestamp(DateTime.Now);
                        c.SendMessageAsync(d, embed: emb.Build());
                        args.Reply("c-createnews-state-success");
                    },
                    GetPlugin());
                },
                GetPlugin());
            },
            GetPlugin());
        }
        */
        [Command("addroleToEveryone", permission = new[] { GuildPermission.Administrator })]
        public async Task Addrole(CommandContext _messageReceviedEventArgs, SocketRole socketRole = null)
        {
            if (socketRole != null)
            {
                var guild = (_messageReceviedEventArgs.Message.Channel as SocketGuildChannel).Guild;
                foreach (SocketGuildUser socketGuildUser in guild.Users)
                {
                    try
                    {
                        if (!socketGuildUser.Roles.Contains(socketRole))
                            await socketGuildUser.AddRoleAsync(socketRole);
                    }
                    catch (Exception e) { }

                }
                await _messageReceviedEventArgs.Message.Channel.SendMessageAsync(":ok: | La commande a été éxécutée avec succès");
            }
        }
        [Command("fban")]
        public async Task FakeBan(CommandContext _eventArgs, SocketUser target, [Remainer][Ignore]string reason = "No reason")
        {
            SocketGuildUser member = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            await
                member
                .SendMessageAsync("c-ban-banned-message".Traduct(new Dictionary<string, string>()
                {
                    ["mention"] = member.Mention,
                    ["server"] = member.Guild.Name,
                    ["kicker"] = _eventArgs.Message.Author.Username,
                    ["raison"] = reason
                }, g, member)).ConfigureAwait(false);
            await _eventArgs.Reply("c-ban-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator,
                ["reason"] = reason
            });
        }

        [Command("mute", isGuildOnly = true, permission = new[] { GuildPermission.Administrator }, permissionsNeed = new[] { GuildPermission.Administrator })]
        public async Task Mute(CommandContext _eventArgs, [Ignore] SocketUser target, [Ignore]string reason = "*no reason*")
        {
            SocketGuildUser member =
                            ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            if (member.GuildPermissions.Has(GuildPermission.ManageMessages) || member.Hierarchy >= g.CurrentUser.Hierarchy || member.Id.Equals(g.OwnerId) || member.Id.Equals(_eventArgs.Message.Author.Id))
            {
                await _eventArgs.Reply("c-mute-cant-mute-moderator", new Dictionary<string, string>()
                {
                    ["user"] = member.Mention
                });
                return;
            }
            foreach (SocketCategoryChannel socketTextChannel in g.CategoryChannels)
            {
                socketTextChannel.AddPermissionOverwriteAsync(target, new OverwritePermissions(sendMessages: PermValue.Deny));

            }
            foreach (SocketTextChannel socketTextChannel1 in g.TextChannels)
            {

                socketTextChannel1.AddPermissionOverwriteAsync(target, new OverwritePermissions(sendMessages: PermValue.Deny));

            }
            await member
            .SendMessageAsync("c-mute-message".Traduct(new Dictionary<string, string>()
            {
                ["mention"] = member.Mention,
                ["server"] = member.Guild.Name,
                ["kicker"] = _eventArgs.Message.Author.Username,
                ["raison"] = reason
            }, g, member));

            await _eventArgs.Reply("c-mute-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator,
                ["reason"] = reason
            });
        }
        [Command("unmute", isGuildOnly = true, permission = new[] { GuildPermission.Administrator }, permissionsNeed = new[] { GuildPermission.Administrator })]
        public async Task UnMute(CommandContext _eventArgs, [Ignore] SocketUser target)
        {
            SocketGuildUser member =
                            ((SocketGuildChannel)_eventArgs.Message.Channel).Guild.GetUser(target.Id);
            SocketGuild g = ((SocketGuildChannel)_eventArgs.Message.Channel).Guild;
            if (member.GuildPermissions.Has(GuildPermission.ManageMessages) || member.Hierarchy >= g.CurrentUser.Hierarchy || member.Id.Equals(g.OwnerId) || member.Id.Equals(_eventArgs.Message.Author.Id))
            {
                await _eventArgs.Reply("c-mute-cant-mute-moderator", new Dictionary<string, string>()
                {
                    ["user"] = member.Mention
                });
                return;
            }
            foreach (SocketCategoryChannel socketTextChannel in g.CategoryChannels)
            {
                if (socketTextChannel.GetPermissionOverwrite(target) != null)
                    socketTextChannel.RemovePermissionOverwriteAsync(target);
            }
            foreach (SocketTextChannel socketTextChannel1 in g.TextChannels)
            {
                if (socketTextChannel1.GetPermissionOverwrite(target) != null)
                    socketTextChannel1.RemovePermissionOverwriteAsync(target);

            }
            await member
            .SendMessageAsync("c-unmute-message".Traduct(new Dictionary<string, string>()
            {
                ["mention"] = member.Mention,
                ["server"] = member.Guild.Name,
                ["kicker"] = _eventArgs.Message.Author.Username
            }, g, member));

            await _eventArgs.Reply("c-unmute-confirmed", new Dictionary<string, string>()
            {
                ["user"] = target.Username + "#" + target.Discriminator
            });
        }

    }
}
