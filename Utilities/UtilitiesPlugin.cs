﻿namespace Utilities
{
    using HeroBot.Plugins.Utilities;
    using HeroBot.Runtime.Entities;
    using System;

    /// <summary>
    /// Defines the <see cref="UtilitiesPlugin" />
    /// </summary>
    public class UtilitiesPlugin : Plugin
    {
        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetName()
        {
            return "Plugins de modération";
        }

        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public override Type[] GetServices()
        {
            return new Type[] { typeof(CommandsService) };
        }

        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetDescription()
        {
            return "Vous permet de modérer votre serveur facilement et efficacement";
        }
    }
}
