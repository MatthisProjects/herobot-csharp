﻿namespace WebHooker
{
    using HeroBot.Runtime;
    using HeroBot.Runtime.Entities;

    /// <summary>
    /// Defines the <see cref="ServiceAgent" />
    /// </summary>
    internal class ServiceAgent : Service
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceAgent"/> class.
        /// </summary>
        /// <param name="sm">The sm<see cref="ServicesManager"/></param>
        /// <param name="plugin">The plugin<see cref="Plugin"/></param>
        public ServiceAgent(ServicesManager sm, Plugin plugin) : base(sm, plugin)
        {
        }

        /// <summary>
        /// The disable
        /// </summary>
        public override void Disable()
        {
        }

        /// <summary>
        /// The enable
        /// </summary>
        public override void Enable()
        {
        }
    }
}
