﻿namespace WebHooker
{
    using HeroBot.Runtime.Entities;
    using System;

    /// <summary>
    /// Defines the <see cref="WebHookerPlugin" />
    /// </summary>
    public class WebHookerPlugin : Plugin
    {
        /// <summary>
        /// The getDescription
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetDescription()
        {
            return "Vous permet d'utiliser HeroBot comme un WebHook compatible avec plufieurs services";
        }

        /// <summary>
        /// The getName
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string GetName()
        {
            return "WebHooker";
        }

        /// <summary>
        /// The getServices
        /// </summary>
        /// <returns>The <see cref="Type[]"/></returns>
        public override Type[] GetServices()
        {
            return new Type[] { typeof(ServiceAgent) };
        }
    }
}
